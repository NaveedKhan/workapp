﻿using RMS.CWR;
using System.Collections.Generic;

namespace CWR_App
{
    class Program
    {
        static void Main(string[] args)
        {
            CWRManager manager = new CWRManager();
            manager.CreateXMLForCWR(new List<int> { 1, 2, 3, 4, 5, 6, 7, 8 });
        }
    }
}
