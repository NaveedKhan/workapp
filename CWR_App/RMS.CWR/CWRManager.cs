﻿using CWR_App.AppDbContext;
using System;
using System.Collections.Generic;
using System.Text;
using CWR_App.CWRCreator;
using CWR_App.HelperMethods;
using System.IO;
using System.Linq;
using System.Xml;
using CWR_App;

namespace RMS.CWR
{
    public class CWRManager
    {
        public CWRManager()
        {

        }

        public void CreateXMLForCWR(List<int> listTrack)
        {
            var dbcontext = new DbContext();
            var lstSocities = dbcontext.Society;
            var firstSociety = lstSocities.FirstOrDefault();
            
            //catalog id is the catalog id  of an artist. As we selected random track ids thats why 
            // thats why i picked catalog id = 1 otherwise we will group track ids with respect to catalog id and then create cwr file
            //EXample below: 
            //var grouptracksByCatalogIds = (from tr in dbcontext.Tracks
            // where listTrack.Contains(tr.TrackID)
            // join ar in dbcontext.artists on tr.ArtistID equals ar.ArtistID
            // select new
            // {
            //  TracksID = tr.TrackID,
            //  CatalogID = ar.CatalogueID
            // }).GroupBy(aa => aa.CatalogID);
            // then enumerate through the list and get the catalog id and the related list of tracks ids 
            int catalogId = 1;



            //every track cwr is created with respect to society id
            //every society has different format and values to create cwr
            //so we are talking society id = 1  or (any first society from db) as our default society
            var SocietyId = firstSociety != null ? firstSociety.Society_ID : 1;
            var versionNumberOfFile = GetVersionNumber(); // this variable is to  get version number to add version number in created cwr file name. 

            var societiesTerritories1 = GetSocietiesTerritories(new List<int> { SocietyId }, catalogId);

            var xml = ExportCwrXml(listTrack, societiesTerritories1, versionNumberOfFile);

            var physicalPath = @"C:\Logs\cwrSample\CWRcreated\";

            if (!Directory.Exists(physicalPath))
            {
                Directory.CreateDirectory(physicalPath);
            }

            var creatcwr = new CwrCreator().Post(xml, physicalPath);
            if (creatcwr.Count > 0)
            {
                Console.WriteLine($"cwr file is created at location {creatcwr.FirstOrDefault().Value}");
                Console.ReadKey();
            }

        }

        private static List<Societies> GetSocieties()
        {
            var dbContext = new DataSeeder();

            var societiesTerritories = (from sdata in dbContext.GetSocieties().Where(a => a.IsActive)
                                        select new Societies
                                        {
                                            SocietyID = sdata.Society_ID,
                                            SocietyName = sdata.Society_Name,
                                            SocietyPerformingCode = sdata.Society_Performing_Code,
                                            SocietyMechanicalCode = sdata.Society_Mechanical_Code
                                        }).ToList();
            return societiesTerritories;
        }

        public static string WritexmlNew(List<FlatExportedData> exportedList, List<Societies> lstSociety, List<SocietiesTerritories> societiesTerritories, string versionNumber)
        {
            try
            {
                var trackWiseData = from data in exportedList
                                    group data by new
                                    {
                                        TracksID = data.TracksId
                                    } into innerGroupData
                                    select innerGroupData;


                XmlDocument doc = new XmlDocument();
                XmlNode scheduleNode = doc.CreateElement("Schedule");
                XmlAttribute scheduleAttr1 = doc.CreateAttribute("SenderId");
                scheduleAttr1.Value = "SEN";
                scheduleNode.Attributes.Append(scheduleAttr1);
                XmlAttribute scheduleAttr2 = doc.CreateAttribute("xmln:sxsi");
                scheduleAttr2.Value = "http://www.w3.org/2001/XMLSchema-instance";
                scheduleNode.Attributes.Append(scheduleAttr2);
                doc.AppendChild(scheduleNode);

                foreach (var item in trackWiseData)
                {
                    var firstTrack = item.FirstOrDefault(aa => aa.TracksId.Value == item.Key.TracksID);
                    var artistname = firstTrack.Artist;
                    var releasedate = firstTrack.ReleaseDate;
                    var catnumber = firstTrack.CatalogNumber;
                    var durationss = firstTrack.DSeconds;
                    var durationmm = firstTrack.DMinutes;
                    var durationhh = firstTrack.DHours;
                    var iswc = firstTrack.Isrc;
                    var title = firstTrack.SongTitle;
                    var id = firstTrack.TracksId;

                    #region Work Node
                    XmlNode workNode = doc.CreateElement("Work");
                    XmlAttribute workAttr1 = doc.CreateAttribute("Artist");
                    workAttr1.Value = artistname;
                    workNode.Attributes.Append(workAttr1);

                    XmlAttribute workAttr2 = doc.CreateAttribute("ReleaseDate");
                    workAttr2.Value = releasedate == null ? "00-00-0000" : releasedate.Value.ToString("dd-MM-yyyy");
                    workNode.Attributes.Append(workAttr2);

                    XmlAttribute workAttr3 = doc.CreateAttribute("CatalogNumber");
                    workAttr3.Value = catnumber;
                    workNode.Attributes.Append(workAttr3);

                    XmlAttribute workAttr4 = doc.CreateAttribute("DurationSS");
                    workAttr4.Value = durationss.ToString();
                    workNode.Attributes.Append(workAttr4);

                    XmlAttribute workAttr5 = doc.CreateAttribute("DurationMM");
                    workAttr5.Value = durationmm.ToString();
                    workNode.Attributes.Append(workAttr5);

                    XmlAttribute workAttr6 = doc.CreateAttribute("DurationHH");
                    workAttr6.Value = durationhh.ToString();
                    workNode.Attributes.Append(workAttr6);

                    XmlAttribute workAttr7 = doc.CreateAttribute("ISWC");
                    workAttr7.Value = iswc;
                    workNode.Attributes.Append(workAttr7);

                    XmlAttribute workAttr8 = doc.CreateAttribute("Title");
                    workAttr8.Value = title;
                    workNode.Attributes.Append(workAttr8);

                    XmlAttribute workAttr9 = doc.CreateAttribute("Id");
                    workAttr9.Value = id.ToString();
                    workNode.Attributes.Append(workAttr9);


                    #endregion

                    var writersdata = item.GroupBy(aa => aa.WriterId).Select(aa => aa.FirstOrDefault()).ToList();
                    XmlNode composers = doc.CreateElement("Composers");
                    foreach (var writer in writersdata)
                    {

                        XmlNode composer = doc.CreateElement("Composer");

                        XmlAttribute composerAttr1 = doc.CreateAttribute("ComposerId");
                        composerAttr1.Value = writer.WriterId.ToString();
                        composer.Attributes.Append(composerAttr1);


                        XmlAttribute composerAttr2 = doc.CreateAttribute("ComposerFirstName");
                        composerAttr2.Value = writer.ComposerFirstName;
                        composer.Attributes.Append(composerAttr2);

                        XmlAttribute composerAttr3 = doc.CreateAttribute("ComposerMiddleName");
                        composerAttr3.Value = writer.ComposerMiddleName;
                        composer.Attributes.Append(composerAttr3);

                        XmlAttribute composerAttr4 = doc.CreateAttribute("ComposerSurname");
                        composerAttr4.Value = writer.ComposerSurName;
                        composer.Attributes.Append(composerAttr4);

                        XmlAttribute composerAttr5 = doc.CreateAttribute("ComposerMOShare");
                        composerAttr5.Value = writer.ComposerMoShare.ToString();
                        composer.Attributes.Append(composerAttr5);

                        XmlAttribute composerAttr6 = doc.CreateAttribute("ComposerMCPSShare");
                        composerAttr6.Value = writer.ComposerMoShare.ToString();
                        composer.Attributes.Append(composerAttr6);

                        XmlAttribute composerAttr7 = doc.CreateAttribute("ComposerPOShare");
                        composerAttr7.Value = writer.ComposerPoShare.ToString();
                        composer.Attributes.Append(composerAttr7);

                        XmlAttribute composerAttr8 = doc.CreateAttribute("ComposerControlled");
                        composerAttr8.Value = writer.ComposerControlled.Value ? "Y" : "N";
                        composer.Attributes.Append(composerAttr8);

                        XmlAttribute composerAttr9 = doc.CreateAttribute("ComposerCAE");
                        composerAttr9.Value = writer.ComposerCaeNumber;
                        composer.Attributes.Append(composerAttr9);

                        XmlAttribute composerAttr10 = doc.CreateAttribute("SongShare");
                        composerAttr10.Value = writer.SongShare.ToString();
                        composer.Attributes.Append(composerAttr10);

                        XmlAttribute composerAttr11 = doc.CreateAttribute("ComposerCapacity");
                        composerAttr11.Value = writer.ComposerCapacity;
                        composer.Attributes.Append(composerAttr11);

                        XmlAttribute composerAttr12 = doc.CreateAttribute("ComposerPRSociety");


                        var proValue = (from s in lstSociety
                                        where s.SocietyID == writer.ComposerPrSociety
                                        select s.SocietyPerformingCode).FirstOrDefault();

                        composerAttr12.Value = string.IsNullOrEmpty(proValue) ? "" : proValue.PadLeft(3, '0');
                        composer.Attributes.Append(composerAttr12);

                        XmlAttribute composerAttr13 = doc.CreateAttribute("ComposerMRSociety");
                        var mrSocValue = (from s in lstSociety
                                          where s.SocietyID == writer.ComposerMrSociety
                                          select s.SocietyMechanicalCode).FirstOrDefault();
                        composerAttr13.Value = string.IsNullOrEmpty(mrSocValue) ? "000" : mrSocValue.PadLeft(3, '0');
                        composer.Attributes.Append(composerAttr13);


                        //PublisherChain node in composer section
                        XmlNode publisherChain = doc.CreateElement("PublisherChain");

                        var hasPublisher = (writer.PublisherId != null && writer.PublisherId != -1
                              && writer.ComposerControlled.Value);
                        if (hasPublisher)
                        {
                            XmlNode publisher = doc.CreateElement("Publisher");

                            XmlAttribute publisherAttr1 = doc.CreateAttribute("PublisherMRSociety");
                            publisherAttr1.Value = writer.PublisherMrSociety;
                            publisher.Attributes.Append(publisherAttr1);

                            XmlAttribute publisherAttr2 = doc.CreateAttribute("PublisherPRSociety");
                            publisherAttr2.Value = writer.PublisherPrSociety;
                            publisher.Attributes.Append(publisherAttr2);

                            XmlAttribute publisherAttr3 = doc.CreateAttribute("PublisherCapacity");
                            publisherAttr3.Value = writer.PublisherCapacity;
                            publisher.Attributes.Append(publisherAttr3);

                            XmlAttribute publisherAttr4 = doc.CreateAttribute("PublisherCAE");
                            publisherAttr4.Value = writer.PublisherCaeNumber;
                            publisher.Attributes.Append(publisherAttr4);

                            XmlAttribute publisherAttr5 = doc.CreateAttribute("PublisherControlled");
                            publisherAttr5.Value = writer.PublisherControlled.Value ? "Y" : "N";
                            publisher.Attributes.Append(publisherAttr5);

                            XmlAttribute publisherAttr6 = doc.CreateAttribute("PublisherPOShare");
                            publisherAttr6.Value = writer.PublisherPoShare.ToString();
                            publisher.Attributes.Append(publisherAttr6);

                            XmlAttribute publisherAttr7 = doc.CreateAttribute("PublisherMOShare");
                            publisherAttr7.Value = writer.PublisherMoShare.ToString();
                            publisher.Attributes.Append(publisherAttr7);

                            XmlAttribute publisherAttr8 = doc.CreateAttribute("PublisherName");
                            publisherAttr8.Value = writer.PublisherName;
                            publisher.Attributes.Append(publisherAttr8);

                            XmlAttribute publisherAttr9 = doc.CreateAttribute("PublisherId");
                            publisherAttr9.Value = writer.PublisherId.ToString();
                            publisher.Attributes.Append(publisherAttr9);

                            publisherChain.AppendChild(publisher);
                        }


                        composer.AppendChild(publisherChain);
                        composers.AppendChild(composer);


                    }
                    workNode.AppendChild(composers);

                    XmlNode exclusions = doc.CreateElement("Exclusions");
                    var exlusionList = (from societiesData in lstSociety
                                        where societiesData.SocietyName != item.FirstOrDefault().SocietyName
                                        select societiesData);

                    workNode.AppendChild(exclusions);
                    scheduleNode.AppendChild(workNode);
                }

                //work node

                XmlNode societies = doc.CreateElement("Societies");
                var groupSocietyByteritory = (from data in societiesTerritories
                                              group data by new
                                              {
                                                  data.SocietyID,
                                                  data.SocietyPerformingCode,
                                                  data.SocietyName,
                                                  data.AgreementNumber
                                              } into td
                                              select td);

                foreach (var groupSociety in groupSocietyByteritory)
                {
                    XmlNode society = doc.CreateElement("Society");

                    XmlAttribute societyAttr1 = doc.CreateAttribute("SocietyId");
                    societyAttr1.Value = groupSociety.Key.SocietyID.ToString();
                    society.Attributes.Append(societyAttr1);

                    XmlAttribute societyAttr2 = doc.CreateAttribute("PerformingCode");
                    societyAttr2.Value = groupSociety.Key.SocietyPerformingCode.ToString();
                    society.Attributes.Append(societyAttr2);

                    XmlAttribute societyAttr3 = doc.CreateAttribute("SocietyName");
                    societyAttr3.Value = groupSociety.Key.SocietyName;
                    society.Attributes.Append(societyAttr3);

                    XmlAttribute societyAttr4 = doc.CreateAttribute("AgreementNumber");
                    societyAttr4.Value = groupSociety.Key.AgreementNumber;
                    society.Attributes.Append(societyAttr4);

                    XmlAttribute societyAttr5 = doc.CreateAttribute("MRSocietyId");
                    societyAttr5.Value = ""; //empty for the time being
                    society.Attributes.Append(societyAttr5);

                    XmlAttribute societyAttr6 = doc.CreateAttribute("VersionNumber");
                    societyAttr6.Value = versionNumber; //your version number
                    society.Attributes.Append(societyAttr6);

                    XmlNode territories = doc.CreateElement("Territories");
                    foreach (var item in groupSociety.ToList())
                    {
                        XmlNode territory = doc.CreateElement("Territory");

                        XmlAttribute territoryAttr1 = doc.CreateAttribute("TerritoryName");
                        territoryAttr1.Value = item.TerritoryName;
                        territory.Attributes.Append(territoryAttr1);

                        XmlAttribute territoryAttr2 = doc.CreateAttribute("TerritoryCode");
                        territoryAttr2.Value = item.TerritoryCode.ToString();
                        territory.Attributes.Append(territoryAttr2);

                        territories.AppendChild(territory);
                    }

                    society.AppendChild(territories);
                    societies.AppendChild(society);
                }

                scheduleNode.AppendChild(societies);
                doc.Save(Console.Out);

                var stringvalue = doc.InnerXml;
                var formatedxml = FormatXml(stringvalue);

                return formatedxml;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return string.Empty;
            }

        }

        public static string FormatXml(string inputXml)
        {
            XmlDocument document = new XmlDocument();
            document.Load(new StringReader(inputXml));

            StringBuilder builder = new StringBuilder();
            using (XmlTextWriter writer = new XmlTextWriter(new StringWriter(builder)))
            {
                writer.Formatting = Formatting.Indented;
                document.Save(writer);
            }

            return builder.ToString();
        }

        public static List<SocietiesTerritories> GetSocietiesTerritories(List<int> lstSocieties, int catalogId)
        {
            var dbContext = new DbContext();

            var societiesTerritories = (from sdata in dbContext.Society
                                        where lstSocieties.Contains(sdata.Society_ID)
                                        join societyterritoryData in dbContext.SocietyTerittory
                                        on sdata.Society_ID equals societyterritoryData.Society_Id
                                        join territoryData in dbContext.Terittory
                                        on societyterritoryData.FkTerritoryId equals territoryData.TerittoryID into tData
                                        join catalogsocietyData in dbContext.CatalogSocieties
                                        on sdata.Society_ID equals catalogsocietyData.SocietyID into cData
                                        from cd in cData.Where(o => o.CatalogID == catalogId)
                                        from td in tData.DefaultIfEmpty()
                                        orderby sdata.Society_ID
                                        select new SocietiesTerritories
                                        {
                                            SocietyID = sdata.Society_ID,
                                            SocietyName = sdata.Society_Name,
                                            Id = td.TerittoryID,
                                            TerritoryCode = td.Territory_Code,
                                            TerritoryName = td.Territory_Name,
                                            SocietyPerformingCode = sdata.Society_Performing_Code,
                                            SocietyMechanicalCode = sdata.Society_Mechanical_Code,
                                            AgreementNumber = cd.AgreementNumber
                                        }).GroupBy(aa => aa.Id).Select(bb => bb.First()).Distinct().ToList();
            return societiesTerritories;
        }

        public static string GetStringTrimingSomeCharacters(string str)
        {
            Encoding ansi = Encoding.GetEncoding(1252);

            Byte[] encodedBytes = ansi.GetBytes(str);
            StringBuilder stringToReturn = new StringBuilder();
            foreach (Byte b in encodedBytes)
            {
                if (((b >= 65) && (b <= 90)) //a-z
                    || ((b >= 97) && (b <= 122)) //A-Z
                    || ((b >= 48) && (b <= 57)) // 0-9
                    || (b == 32) //
                    || (b == 33) //!
                    || (b == 35) //#
                    || (b == 36) //$
                    || (b == 38) //&
                    || (b == 39) //'
                    || (b == 40) //(
                    || (b == 41) //)
                    || (b == 43) //+
                    || (b == 45) //-
                    || (b == 46) //.
                    || (b == 47) // /
                    || (b == 63) //?
                    || (b == 64) // @
                    )
                    stringToReturn.Append((char)b);
                else
                    stringToReturn.Append(" ");
            }
            return stringToReturn.ToString();
        }


        public static string ExportCwrXml(List<int> lstTracks, List<SocietiesTerritories> societiesTerritories, string versionNumber)
        {
            try
            {
                #region get data

                var exportedList = new List<FlatExportedData>();
                List<Societies> lstSociety = GetSocieties();
                GC.WaitForPendingFinalizers();
                exportedList = GetFlatExportedData(lstTracks);

                #endregion get data

                var xml = WritexmlNew(exportedList, lstSociety, societiesTerritories, versionNumber);
                return xml.ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        private static string GetVersionNumber()
        {
            return "422";
        }

        public static List<FlatExportedData> GetFlatExportedData(List<int> lstTracks)
        {
            var dbContext = new DbContext();

            var exportedData = from trackData in dbContext.Tracks
                               where lstTracks.Contains(trackData.TrackID)
                               join artistData in dbContext.artists
                               on trackData.ArtistID equals artistData.ArtistID
                               join catalogData in dbContext.Catalog
                               on artistData.CatalogueID equals catalogData.Catalogue_ID
                               join publisherCatalogData in dbContext.CatalogPublishers
                               on catalogData.Catalogue_ID equals publisherCatalogData.fkCatalogID
                               join trackWriterData in dbContext.TrackWriter
                               on trackData.TrackID equals trackWriterData.TrackID
                               join writerData in dbContext.Writers
                               on trackWriterData.WriterID equals writerData.WriterId
                               join trackRegistrationData in dbContext.TrackRegistration
                               on trackData.TrackID equals trackRegistrationData.TrackID
                               join societyData in dbContext.Society
                               on trackRegistrationData.SocietyID equals societyData.Society_ID
                               join writerregistration in dbContext.WriterRegistration
                                on writerData.WriterId equals writerregistration.WriterID into writerreg
                               join pData in dbContext.Publishers
                               on publisherCatalogData.fkPublisherID equals pData.Publisher_ID into publisherJoinedData
                               from wreg in writerreg.DefaultIfEmpty()
                               from publisherData in publisherJoinedData.DefaultIfEmpty()
                               where trackWriterData.SongShare > 0

                               select new FlatExportedData
                               {
                                   ArtistId = artistData.ArtistID,
                                   SongTitle = trackData.SongTitle,
                                   TracksId = trackData.TrackID,
                                   NewTracksId = trackData.NewTrackID,
                                   WriterId = writerData.WriterId,
                                   Artist = artistData.BandName,
                                   CatalogNumber = trackData.CatalogueNumber,
                                   ReleaseDate = trackData.ReleaseDate,
                                   Isrc = trackData.ISWCNumber,
                                   RegistrationDate = trackRegistrationData.CreatedDate,
                                   SocietyId = trackRegistrationData.SocietyID,
                                   SocietyName = societyData.Society_Name,
                                   TuneCode = trackRegistrationData.Tunecode,
                                   DHours = trackData.DHours,
                                   DMinutes = trackData.DMinutes,
                                   DSeconds = trackData.DSeconds,
                                   SongShare = trackWriterData.SongShare,
                                   ComposerControlled = trackWriterData.IsRightToCollect,
                                   ComposerCaeNumber = writerData.CAENo,
                                   ComposerCapacity = trackWriterData.ComposerCapacity_WriterRole,
                                   ComposerFirstName = writerData.FirstName,
                                   ComposerSurName = writerData.SurName,
                                   ComposerMoShare = trackWriterData.ComposerMoShare_MCPSShare,
                                   ComposerPoShare = trackWriterData.SongShare,
                                   ComposerMiddleName = writerData.MiddleName,

                                   PublisherId = publisherData.Publisher_ID,
                                   PublisherCaeNumber = publisherData.CAE_Number,
                                   PublisherName = publisherData.Publisher_Name,
                                   PublisherControlled = trackWriterData.PublisherControlled,
                                   PublisherMoShare = 0,// trackWriterData.PublisherMOShare,
                                   PublisherPoShare = 0,// trackWriterData.PublisherPOShare,
                                   PublisherCapacity = trackWriterData.PublisherCapacity,
                                   ComposerPrSociety = (wreg != null && wreg.PROSocietyID != null) ? wreg.PROSocietyID : 0,
                                   ComposerMrSociety = (wreg != null && wreg.MechSocietyID != null) ? wreg.MechSocietyID : 0,
                                   PublisherMrSociety = publisherData.PublisherMRSociety == null ? string.Empty : publisherData.PublisherMRSociety.ToString(),
                                   PublisherPrSociety = publisherData.PublisherPRSociety == null ? string.Empty : publisherData.PublisherPRSociety.ToString(),
                               };
            return exportedData.ToList();
        }
    }
}
