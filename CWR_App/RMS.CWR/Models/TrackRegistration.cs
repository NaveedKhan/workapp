﻿using System;

namespace CWR_App.Models
{
    public class TrackRegistration
    {

        public int TrackRegistrationID { get; set; }
        public int SocietyID { get; set; }
        public string Tunecode { get; set; }
        public int TrackID { get; set; }
        public DateTime? CreatedDate { get; set; }
        
    }
}
