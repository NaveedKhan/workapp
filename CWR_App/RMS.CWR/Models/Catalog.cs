﻿namespace CWR_App.Models
{
    public class Catalog
    {
        public int Catalogue_ID { get; set; }
        public string Catalogue_Name { get; set; }
        public string POShare { get; set; }
        public string MOShare { get; set; }
    }
}
