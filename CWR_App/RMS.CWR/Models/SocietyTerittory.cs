﻿namespace CWR_App.Models
{
    public class SocietyTerittory
    {
        public int SocietyTerittoriID { get; set; }
        public int Society_Id { get; set; }
        public int FkTerritoryId { get; set; }
        public int FkCountryId { get; set; }
    }
}
