﻿namespace CWR_App.Models
{
    public class Publishers
    {
        public int Publisher_ID { get; set; }
        public string CAE_Number { get; set; }
        public string Publisher_Name { get; set; }
        public int? PublisherMRSociety { get; set; }
        public int? PublisherPRSociety { get; set; }
    }
}
