﻿namespace CWR_App.Models
{
    public class TrackWriter
    {
        public int TrackWriterID { get; set; }
        public int SongShare { get; set; }
        public string ComposerCapacity_WriterRole { get; set; }
        public bool ComposerControlled_IsRightToCollect { get; set; }
        public int ComposerMoShare_MCPSShare { get; set; }
        public int ComposerPoShare_SongShare { get; set; }
        public bool PublisherControlled { get; set; }
        public bool IsRightToCollect { get; set; }
        public int PublisherMoShare { get; set; }
        public int PublisherPoShare { get; set; }
        public string PublisherCapacity { get; set; }
        public int TrackID { get; set; }
        public int WriterID { get; set; }
    }
}
