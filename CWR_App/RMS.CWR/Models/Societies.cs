﻿// ReSharper disable once CheckNamespace
namespace CWR_App
{
    public class Societies
    {
        public int SocietyID { get; set; }
        public string SocietyName { get; set; }
        public string SocietyPerformingCode { get; set; }
        public string SocietyMechanicalCode { get; set; }
    }
}