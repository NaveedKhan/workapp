﻿namespace CWR_App.Models
{
    public class Writers
    {
        public int WriterId { get; set; }
        public string SurName { get; set; }
        public string MiddleName { get; set; }
        public string FirstName { get; set; }
        public string CAENo { get; set; }
        public int fkArtistID { get; set; }
        public bool ISMemberOfPRS { get; set; }
        public bool ISMemberOfMCPS { get; set; }

    }
}
