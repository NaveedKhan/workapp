﻿namespace CWR_App.Models
{
    public class Country
    {
        public int CountryID { get; set; }
        public string CountryName { get; set; }
        public string iso { get; set; }
        public string numcode { get; set; }
        public string DialingCode { get; set; }
        public string Continent { get; set; }
        public string iso3 { get; set; }
    }
}
