﻿namespace CWR_App
{
    public class Smwriters
    {
        public int WriterId { get; set; }
        public int SurName { get; set; }
        public int MiddleName { get; set; }
        public int FirstName { get; set; }
        public int CAENo { get; set; }
        public int fk_ModificationStatusId { get; set; }
        public int fkArtistID { get; set; }
        public int GroupId { get; set; }
        public int FkPreviousStatus { get; set; }
        public int ISMemberOfPRS { get; set; }
        public int ISMemberOfMCPS { get; set; }
        public int DbId { get; set; }
        public int ComposerAffiliation { get; set; }
        public int fkTransferHistory { get; set; }

    }
}