﻿namespace CWR_App.Models
{
    public class Artist
    {
        public int ArtistID { get; set; }
        public string BandName { get; set; }
        public int CatalogueID { get; set; }
        public int CountryID { get; set; }
    }
}
