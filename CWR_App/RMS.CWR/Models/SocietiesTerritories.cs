﻿// ReSharper disable once CheckNamespace
namespace CWR_App
{
    public class SocietiesTerritories
    {
        public int Id { get; set; }
        public int SocietyID { get; set; }
        public string SocietyName { get; set; }
        public string TerritoryCode { get; set; }
        public string TerritoryName { get; set; }
        public string SocietyPerformingCode { get; set; }
        public string SocietyMechanicalCode { get; set; }

        public string AgreementNumber { get; set; }
    }
}