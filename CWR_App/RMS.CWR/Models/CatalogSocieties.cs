﻿namespace CWR_App.Models
{
    public class CatalogSocieties
    {

        public int CatalogSocietiesID { get; set; }
        public int CatalogID { get; set; }
        public int SocietyID { get; set; }
        public string AgreementNumber { get; set; }

    }
}
