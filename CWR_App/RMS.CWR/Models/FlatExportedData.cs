﻿using System;
using System.Collections.Generic;

namespace CWR_App
{ 
    public class FlatExportedData
    {
        public string PublisherName { get; set; }
        public int? TracksId { get; set; }
        public int? NewTracksId { get; set; }

        public string MassTrax_TrackNo { get; set; }
        public int? ComposerPrSociety { get; set; }
        public string ComposerMiddleName { get; set; }
        public string ComposerSurName { get; set; }
        public double? ComposerPoShare { get; set; }
        public double? ComposerMoShare { get; set; }
        public string ComposerCaeNumber { get; set; }
        public string ComposerCapacity { get; set; }
        public int? ComposerMrSociety { get; set; }
        public bool? ComposerControlled { get; set; }
        public string ComposerLinkedPublisher { get; set; }
        public string ComposerFirstName { get; set; }
        public string ComposerAffiliation { get; set; }

        public int? WriterId { get; set; }
        public string SongTitle { get; set; }
        public string AlternateTitle { get; set; }
        public string SongNotes { get; set; }
        public string Artist { get; set; }
        public string ReleaseDateCwr { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public string RecordLabel { get; set; }
        public string CatalogNumber { get; set; }
        public string Isrc { get; set; }
        public string Iswc { get; set; }
        public string Recorded { get; set; }

        public string PublisherCaeNumber { get; set; }
        public bool? PublisherControlled { get; set; }
        public string PublisherLinkedPublisher { get; set; }
        public double? PublisherMoShare { get; set; }
        public double? PublisherPoShare { get; set; }
        public string PublisherCapacity { get; set; }
        public int? PublisherId { get; set; }
        public string PublisherMrSociety { get; set; }
        public string PublisherPrSociety { get; set; }

        public int? SocietyId { get; set; }
        public string SocietyName { get; set; }
        public string TuneCode { get; set; }
        public DateTime? RegistrationDate { get; set; }
        public string AkAsName { get; set; }

        public int? ThirdPartyPublisherId { get; set; }

        public string ThirdPartyPublisherName { get; set; }
        public int? TerritoryId { get; set; }
        public string TerritoryName { get; set; }
        public string TerritoryCode { get; set; }
        public int? DHours { get; set; }
        public int? DMinutes { get; set; }
        public int? DSeconds { get; set; }

        public int? Writersociety { get; set; }

        //sm-5294
        public int? Writerprosociety { get; set; }

        public double? SongShare { get; set; }
        public DateTime? CreateDate { get; set; }
        public string ComposerName { get; set; }
        public string ComposerProSocietyName { get; set; }
        public List<Smwriters> Composers { get; set; }
		
        public int? ArtistId { get; set; }
    }
}