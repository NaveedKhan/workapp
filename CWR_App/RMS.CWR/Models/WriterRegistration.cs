﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CWR_App.Models
{
   public class WriterRegistration
    {
        public int WriterRegistrationID { get; set; }
        public int WriterID { get; set; }
        public int? PROSocietyID { get; set; }
        public int? MechSocietyID { get; set; }
    }
}
