﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CWR_App.Models.CWRModels
{
    public partial class cwrrecordformat
    {
        public int Id { get; set; }
        public string RecordPrefix { get; set; }
        public string TransactionSequence { get; set; }
        public string RecordSequence { get; set; }
        public string WorkTitle { get; set; }
        public string LanguageCode { get; set; }
        public string WorkId { get; set; }
        public string ISWC { get; set; }
        public string CopyrightDate { get; set; }
        public string CopyrightNumber { get; set; }
        public string DistributionCategory { get; set; }
        public string Duration { get; set; }
        public string Recorded { get; set; }
        public string TextMusicRelationship { get; set; }
        public string CompositeType { get; set; }
        public string VersionType { get; set; }
        public string ExcerptType { get; set; }
        public string MusicArrangement { get; set; }
        public string LyricAdaption { get; set; }
        public string ContactName { get; set; }
        public string ContactId { get; set; }
        public string WorkType { get; set; }
        public string GrandRights { get; set; }
        public string CompositeComponentCount { get; set; }
        public string PrintPublicationDate { get; set; }
        public string ExceptionalClause { get; set; }
        public string OpusNumber { get; set; }
        public string CatalogueNumber { get; set; }
        public string PriorityFlag { get; set; }
    }
}
