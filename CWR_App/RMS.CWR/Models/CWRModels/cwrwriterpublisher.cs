﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CWR_App.Models.CWRModels
{
    public partial class cwrwriterpublisher
    {
        public int Id { get; set; }
        public string RecordPrefix { get; set; }
        public string TransactionSequence { get; set; }
        public string RecordSequence { get; set; }
        public string PublisherIP { get; set; }
        public string PublisherName { get; set; }
        public string SubmitterAgreementNumber { get; set; }
        public string SocietyAssignedAgreementNumber { get; set; }
        public string WriterIP { get; set; }
    }
}
