﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CWR_App.Models.CWRModels
{
    public partial class cwrtemplate
    {
        public int Id { get; set; }
        public int HeaderId { get; set; }
        public int GroupHeaderId { get; set; }
        public int GroupTrailerId { get; set; }
        public int TrailerId { get; set; }

        public virtual header header { get; set; }
        public virtual groupheader groupheader { get; set; }
        public virtual grouptrailer grouptrailer { get; set; }
        public virtual trailer trailer { get; set; }
    }
}
