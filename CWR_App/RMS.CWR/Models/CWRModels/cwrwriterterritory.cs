﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CWR_App.Models.CWRModels
{
    public partial class cwrwriterterritory
    {
        public int Id { get; set; }
        public string RecordPrefix { get; set; }
        public string TransactionSequence { get; set; }
        public string RecordSequence { get; set; }
        public string InterestedParty { get; set; }
        public string PRCollectionShare { get; set; }
        public string MRCollectionShare { get; set; }
        public string SRCollectionShare { get; set; }
        public string Inclusion_Exclusion { get; set; }
        public string TerritoryCode { get; set; }
        public string SharesChange { get; set; }
        public string Sequence { get; set; }
    }
}
