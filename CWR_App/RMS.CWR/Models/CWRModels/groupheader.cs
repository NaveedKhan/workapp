﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CWR_App.Models.CWRModels
{
    public partial class groupheader
    {
        public groupheader()
        {
            this.cwrtemplates = new HashSet<cwrtemplate>();
        }

        public int Id { get; set; }
        public string RecordType { get; set; }
        public string TransactionType { get; set; }
        public string GroupId { get; set; }
        public string VersionNumber { get; set; }
        public string BatchRequest { get; set; }
        public string SubmissionType { get; set; }

        public virtual ICollection<cwrtemplate> cwrtemplates { get; set; }
    }
}
