﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CWR_App.Models.CWRModels
{
    public partial class cwrpublisher
    {
        public int Id { get; set; }
        public string RecordPrefix { get; set; }
        public string TransactionSequence { get; set; }
        public string RecordSequence { get; set; }
        public string PublisherSequence { get; set; }
        public string InterestedParty { get; set; }
        public string PublisherName { get; set; }
        public string UnknownPublisher { get; set; }
        public string PublisherType { get; set; }
        public string Tax_ID { get; set; }
        public string PublisherCAE { get; set; }
        public string SubmitterAgreementNumber { get; set; }
        public string PRSocietyId { get; set; }
        public string PROwnershipShare { get; set; }
        public string MRSocietyId { get; set; }
        public string MROwnershipShare { get; set; }
        public string SRSocietyId { get; set; }
        public string SROwnershipShare { get; set; }
        public string SpecialAgreements { get; set; }
        public string Refusal { get; set; }
        public string Filler { get; set; }
        public string PublisherIPI { get; set; }
        public string ISAC { get; set; }
        public string SocietyAssignedAgreementNumber { get; set; }
        public string AgreementType { get; set; }
        public string USALicense { get; set; }
    }
}
