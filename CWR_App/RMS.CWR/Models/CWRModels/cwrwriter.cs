﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CWR_App.Models.CWRModels
{
    public partial class cwrwriter
    {
        public int Id { get; set; }
        public string RecordPrefix { get; set; }
        public string TransactionSequence { get; set; }
        public string RecordSequence { get; set; }
        public string InterestedParty { get; set; }
        public string WriterLastName { get; set; }
        public string WriterFirstName { get; set; }
        public string WriterUnknown { get; set; }
        public string WriterDesignationCode { get; set; }
        public string TaxID { get; set; }
        public string WriterCAE { get; set; }
        public string PRSociety { get; set; }
        public string PRShare { get; set; }
        public string MRSociety { get; set; }
        public string MRShare { get; set; }
        public string SRSociety { get; set; }
        public string SRShare { get; set; }
        public string Reversionary { get; set; }
        public string Refusal { get; set; }
        public string WorkHire { get; set; }
        public string Filler { get; set; }
        public string WriterIPI { get; set; }
        public string PersonalNumber { get; set; }
        public string USALicense { get; set; }
    }
}
