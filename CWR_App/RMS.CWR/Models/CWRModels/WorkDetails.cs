﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CWR_App.Models.CWRModels
{
    public class WorkDetails
    {
        public string WorkLine { get; set; }
        public string WorkId { get; set; }
    }
}
