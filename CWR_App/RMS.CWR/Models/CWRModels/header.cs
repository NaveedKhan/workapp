﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CWR_App.Models.CWRModels
{
    public partial class header
    {
        public header()
        {
            this.cwrtemplates = new HashSet<cwrtemplate>();
        }

        public int Id { get; set; }
        public string RecordType { get; set; }
        public string SenderType { get; set; }
        public string SenderId { get; set; }
        public string SenderName { get; set; }
        public string EDINumber { get; set; }
        public string CreationDate { get; set; }
        public string CreationTime { get; set; }
        public string TransmissionDate { get; set; }

        public virtual ICollection<cwrtemplate> cwrtemplates { get; set; }
    }
}
