﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CWR_App.Models.CWRModels
{
    public partial class grouptrailer
    {
        public grouptrailer()
        {
            this.cwrtemplates = new HashSet<cwrtemplate>();
        }

        public int Id { get; set; }
        public string RecordType { get; set; }
        public string GroupId { get; set; }
        public string TransactionCount { get; set; }
        public string RecordCount { get; set; }

        public virtual ICollection<cwrtemplate> cwrtemplates { get; set; }
    }
}
