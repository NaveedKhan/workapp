﻿namespace CWR_App.Models
{
    public class Society
    {

        public int Society_ID { get; set; }
        public string Society_Name { get; set; }
        public string Mechanical_Society_Name { get; set; }
        public string Society_Performing_Code { get; set; }
        public string Society_Mechanical_Code { get; set; }
        public bool IsActive { get; set; }

    }
}
