﻿using System;

namespace CWR_App.Models
{
    public class Tracks
    {
        public int TrackID { get; set; }
        public string SongTitle { get; set; }
        public int NewTrackID { get; set; }
        public int ArtistID { get; set; }
        public string ISWCNumber { get; set; }
        public DateTime ReleaseDate { get; set; }
        public int DHours { get; set; }
        public int DMinutes { get; set; }
        public int DSeconds { get; set; }
        public string CatalogueNumber { get; set; }

    }
}
