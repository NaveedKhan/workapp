﻿using CWR_App;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;


namespace BAL.MaestroExcel
{
    public interface IMaestroExcel
    {
    }
    public class ImpPublisher
    {
        public string PublisherName { get; set; }

        public string PublisherPoShare { get; set; }

        public string PublisherMoShare { get; set; }

        public string PublisherCaeNo { get; set; }

        public string PublisherCapacity { get; set; }

        public string PublisherControlled { get; set; }

        public string PublisherPrSociety { get; set; }
    }

    public class ImpComposer
    {
        public string ComposerFirstName { get; set; }
        public List<ImpPublisher> LstPublisher { get; set; }

        public string ComposerMiddleName { get; set; }

        public string ComposerPoShare { get; set; }

        public string ComposerMoShare { get; set; }

        public string ComposerCaeNo { get; set; }

        public string ComposerAffiliation { get; set; }

        public string ComposerControlled { get; set; }

        public string ComposerCapacity { get; set; }

        public string ComposerSurname { get; set; }
    }

    public class ImpTrack
    {
        public List<ImpComposer> LstComposer { get; set; }
        public string SongTitle { get; set; }
        public List<string> LstAkAs { get; set; }
        public List<string> LstAlternateTitles { get; set; }
        public string Isrc { get; set; }
        public string SongCode { get; set; }
        public int DurationHh { get; set; }
        public int DurationMm { get; set; }
        public int DurationSs { get; set; }
        public string CatalogNumber { get; set; }
        public string ReleaseDate { get; set; }
        public string Artist { get; set; }
    }

    public class MaestroExcel : IMaestroExcel
    {
        
        
     

      
    }
}