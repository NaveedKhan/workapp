﻿namespace BAL.MaestroExcel
{
    public class PublisherInformation
    {
        public string PublisherName { get; set; }

        public double? PublisherMoShare { get; set; }
        public double? PublisherPoShare { get; set; }
        public bool? PublisherControlled { get; set; }
        public string PublisherCaeNo { get; set; }

        public string PublisherCapacity { get; set; }
        public string PublisherLinkedPublisher { get; set; }
        public string PublisherPrSociety { get; set; }
    }

    public class AkAs
    {
        public string AkaName { get; set; }
    }
}