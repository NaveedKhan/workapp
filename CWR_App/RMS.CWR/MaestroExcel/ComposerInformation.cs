﻿using System.Collections.Generic;

namespace BAL.MaestroExcel
{
    public class ComposerInformation
    {
        public string ComposerName { get; set; }
        public string ComposerFirstName { get; set; }
        public string ComposerSurname { get; set; }
        public string ComposerMiddleName { get; set; }
        public double? ComposerMoShare { get; set; }
        public double? ComposerPoShare { get; set; }
        public bool? ComposerControlled { get; set; }
        public string ComposerCaeNo { get; set; }
        public string ComposerCapacity { get; set; }

        public string ComposerLinkedPublisher { get; set; }

        //sm-5294
        public int? Writerprosociety { get; set; }

        public List<PublisherInformation> LstPublisherInformation { get; set; }
        public string ComposerProSocietyName { get;  set; }
    }
}