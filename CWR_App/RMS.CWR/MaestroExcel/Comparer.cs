﻿using BAL.MaestroExcel;
using System.Collections.Generic;
using System.Xml.Linq;

namespace BAL
{
    public class TerritoriesComparer : IEqualityComparer<XElement>
    {
        public bool Equals(XElement x, XElement y)
        {
            return x.Attribute("TerritoryCode").Value == y.Attribute("TerritoryCode").Value;
        }

        public int GetHashCode(XElement obj)
        {
            return base.GetHashCode();
        }
    }

    public class AkaComparer : IEqualityComparer<XElement>
    {
        public bool Equals(XElement x, XElement y)
        {
            return x.Attribute("AKAsName").Value == y.Attribute("AKAsName").Value;
        }

        public int GetHashCode(XElement obj)
        {
            return base.GetHashCode();
        }
    }

    //TODO: not referenced anywhere
    public class AlternateTitleComparer : IEqualityComparer<XElement>
    {
        public bool Equals(XElement x, XElement y)
        {
            return x.Attribute("AlternateTitle").Value == y.Attribute("AlternateTitle").Value;
        }

        public int GetHashCode(XElement obj)
        {
            return base.GetHashCode();
        }
    }

    public class ComposerComparer : IEqualityComparer<XElement>
    {
        public bool Equals(XElement x, XElement y)
        {
            return x.Attribute("ComposerId").Value == y.Attribute("ComposerId").Value;
        }

        public int GetHashCode(XElement obj)
        {
            return base.GetHashCode();
        }
    }

    public class PublisherComparer : IEqualityComparer<XElement>
    {
        public bool Equals(XElement x, XElement y)
        {
            return x.Attribute("PublisherId").Value == y.Attribute("PublisherId").Value;
        }

        public int GetHashCode(XElement obj)
        {
            return base.GetHashCode();
        }
    }

    public class ComposerInformationComparer : IEqualityComparer<ComposerInformation>
    {
        public bool Equals(ComposerInformation x, ComposerInformation y)
        {
            return x.ComposerName == y.ComposerName;
        }

        public int GetHashCode(ComposerInformation obj)
        {
            return base.GetHashCode();
        }
    }

    public class PublisherInformationComparer : IEqualityComparer<PublisherInformation>
    {
        public bool Equals(PublisherInformation x, PublisherInformation y)
        {
            return x.PublisherName == y.PublisherName;
        }

        public int GetHashCode(PublisherInformation obj)
        {
            return base.GetHashCode();
        }
    }

    public class AkAsComparer : IEqualityComparer<AkAs>
    {
        public bool Equals(AkAs x, AkAs y)
        {
            return x.AkaName == y.AkaName;
        }

        public int GetHashCode(AkAs obj)
        {
            return base.GetHashCode();
        }
    }

    public class AlternateTitlesComparer : IEqualityComparer<AlternateTitles>
    {
        public bool Equals(AlternateTitles x, AlternateTitles y)
        {
            return x.AlternateTitle == y.AlternateTitle;
        }

        public int GetHashCode(AlternateTitles obj)
        {
            return base.GetHashCode();
        }
    }

}

public enum CwrDb
{
    Sentric = 1,
    Import = 2,
    Maestro = 3
}