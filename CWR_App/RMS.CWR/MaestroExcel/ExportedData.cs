﻿using System;
using System.Collections.Generic;

namespace BAL.MaestroExcel
{
    public class ExportedData
    {
        public int PublisherCount { get; set; }
        public int ComposerCount { get; set; }
        public int TitleCount { get; set; }
        public int AkaCount { get; set; }
        public int? SongCode { get; set; }
        public string SongTitle { get; set; }

        public string ArtistCwr { get; set; }
        public string ReleaseDateCwr { get; set; }
        public DateTime? FirstReleaseDate { get; set; }
        public string RecordLabel { get; set; }
        public string CatalogNumber { get; set; }
        public string Isrc { get; set; }
        public string Iswc { get; set; }
        public string Recorded { get; set; }

        public List<ComposerInformation> LstComposerInformation { get; set; }

        public List<AlternateTitles> LstTracksAlternateTitles { get; set; }
        public List<AkAs> LstAkAs { get; set; }

        public string SongNotes { get; set; }

        public int? DHours { get; set; }
        public int? DMinutes { get; set; }
        public int? DSeconds { get; set; }
        public double? SongShare { get; set; }
        public int? Writersociety { get; set; }
        public DateTime? CreateDate { get;  set; }
    }
}