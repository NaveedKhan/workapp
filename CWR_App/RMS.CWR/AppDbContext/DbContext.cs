﻿using CWR_App.HelperMethods;
using CWR_App.Models;
using CWR_App.Models.CWRModels;
using System.Collections.Generic;

namespace CWR_App.AppDbContext
{
    public class DbContext
    {
        private readonly DataSeeder dataseeder;

        #region Properties

     

        public List<Society> Society
        {
            get
            {
                return dataseeder.GetSocieties();
            }
        }


        public List<CatalogSocieties> CatalogSocieties
        {
            get
            {
                return dataseeder.GetCatalogSocieties();
            }
        }


        public List<Terittory> Terittory
        {
            get
            {
                return dataseeder.GetTeritory();
            }
        }

        public List<Country> Country
        {
            get
            {
                return dataseeder.GetCountry();
            }
        }

        public List<SocietyTerittory> SocietyTerittory
        {
            get
            {
                return dataseeder.GetSocietyTerittory();
            }
        }

        public List<Publishers> Publishers
        {
            get
            {
                return dataseeder.GetPublishers();
            }
        }


        public List<CatalogPublishers> CatalogPublishers
        {
            get
            {
                return dataseeder.GetCatalogPublishers();
            }
        }

        public List<Tracks> Tracks
        {
            get
            {
                return dataseeder.GetTracks();
            }
        }

        public List<TrackRegistration> TrackRegistration
        {
            get
            {
                return dataseeder.GetTrackRegistration();
            }
        }

        public List<Writers> Writers
        {
            get
            {
                return dataseeder.GetWriters();
            }
        }
        public List<WriterRegistration> WriterRegistration
        {
            get
            {
                return dataseeder.GetWriterRegistration();
            }
        }

        public List<TrackWriter> TrackWriter
        {
            get
            {
                return dataseeder.GetTrackWriters();
            }
        }

        public List<Catalog> Catalog
        {
            get
            {
                return dataseeder.GetCatalogue();
            }
        }
        public List<cwrterritory> cwrterritory
        {
            get
            {
                return dataseeder.GetcwrTerritory();
            }
        }
        public List<cwrpublisher> cwrpublisher
        {
            get
            {
                return dataseeder.GetcwrPublisher();
            }
        }
        public List<cwrwriterterritory> cwrwriterterritory
        {
            get
            {
                return dataseeder.Getcwrwriterterritory();
            }
        }
        public List<cwrwriter> cwrwriter
        {
            get
            {
                return dataseeder.Getcwrwriter();
            }
        }
        public List<cwrwriterpublisher> cwrwriterpublisher
        {
            get
            {
                return dataseeder.Getcwrwriterpublisher();
            }
        }
        public List<cwrrecordformat> cwrrecordformat
        {
            get
            {
                return dataseeder.Getcwrrecordformat();
            }
        }
        public List<cwrtemplate> cwrtemplate
        {
            get
            {
                return dataseeder.Getcwrtemplates();
            }
        }


        //tracks and artist related properties
        public List<Artist> artists
        {
            get
            {
                return dataseeder.GetArtists();
            }
        }

        public List<Catalog> catalogues
        {
            get
            {
                return dataseeder.GetCatalogue();
            }
        }
        #endregion


        public DbContext()
        {
            dataseeder = new DataSeeder();
        }

    }
}
