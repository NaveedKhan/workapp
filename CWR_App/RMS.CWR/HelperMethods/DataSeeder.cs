﻿using CWR_App.Models;
using CWR_App.Models.CWRModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;

namespace CWR_App.HelperMethods
{
    public class DataSeeder
    {

       private readonly string  path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "CWRDB");

        public List<Artist> GetArtists()
        {
            var artistsList = new List<Artist>();

            foreach (var item in new List<int> { 1, 2, 3, 4, 5 })
            {
                var artist = new Artist
                {
                    ArtistID = item,
                    BandName = $"Artist{item}",
                    CatalogueID = item,
                    CountryID = item
                };

                artistsList.Add(artist);
            }
            return artistsList;
        }


        public List<Catalog> GetCatalogue()
        {
            var xmlfilepath = Path.Combine(path, @"TracksRelated\catalog.xml");

            XDocument xdoc = XDocument.Load(xmlfilepath);
            var lv1s = (from lv1 in xdoc.Descendants("RECORD")
                        select new Catalog
                        {
                            Catalogue_Name = lv1.Attribute("Catalogue_Name").Value,
                            Catalogue_ID = (int)lv1.Attribute("Catalogue_ID"),
                            MOShare = lv1.Attribute("MOShare").Value,
                            POShare = lv1.Attribute("POShare").Value,
                        }).ToList();

            return lv1s;
        }

        public List<Society> GetSocieties()
        {

            var xmlfilepath = Path.Combine(path, @"TracksRelated\society.xml");

            XDocument xdoc = XDocument.Load(xmlfilepath);
            var lv1s = (from lv1 in xdoc.Descendants("RECORD")
                        select new Society
                        {
                            IsActive = true,
                            Society_ID = (int)lv1.Attribute("Society_ID"),
                            Society_Name = lv1.Attribute("Society_Name").Value,
                            Mechanical_Society_Name = lv1.Attribute("Mechanical_Society_Name").Value,
                            Society_Mechanical_Code = lv1.Attribute("Society_Performing_Code").Value,
                            Society_Performing_Code = lv1.Attribute("Society_Mechanical_Code").Value,
                        }).ToList();

            return lv1s;
        }

        public List<CatalogSocieties> GetCatalogSocieties()
        {

            var xmlfilepath = Path.Combine(path, @"TracksRelated\catalogsocieties.xml");

            XDocument xdoc = XDocument.Load(xmlfilepath);
            var lv1s = (from lv1 in xdoc.Descendants("RECORD")
                        select new CatalogSocieties
                        {
                            AgreementNumber = RandomString(), // lv1.Attribute("AgreementNumber").Value,
                            CatalogSocietiesID = (int)lv1.Attribute("ID"),
                            CatalogID = (int)lv1.Attribute("fkCatalogID"),
                            SocietyID = (int)lv1.Attribute("fkSocietyID"),
                        }).ToList();

            return lv1s;
        }


        public List<Terittory> GetTeritory()
        {

            var xmlfilepath = Path.Combine(path, @"TracksRelated\territory.xml");
            XDocument xdoc = XDocument.Load(xmlfilepath);
            var lv1s = (from lv1 in xdoc.Descendants("RECORD")
                        select new Terittory
                        {
                            Territory_Code = lv1.Attribute("Territory_Code").Value,
                            TerittoryID = (int)lv1.Attribute("ID"),
                            Territory_Name = lv1.Attribute("Territory_Name").Value,
                        }).ToList();

            return lv1s;
        }

        public List<Country> GetCountry()
        {
            var xmlfilepath = Path.Combine(path, @"TracksRelated\country.xml");
            XDocument xdoc = XDocument.Load(xmlfilepath);
            var lv1s = (from lv1 in xdoc.Descendants("RECORD")
                        select new Country
                        {
                            Continent = lv1.Attribute("Continent").Value,
                            CountryID = (int)lv1.Attribute("CountryID"),
                            CountryName = lv1.Attribute("name").Value,
                            DialingCode = lv1.Attribute("DialingCode").Value,
                            iso = lv1.Attribute("iso").Value,
                            numcode = lv1.Attribute("numcode").Value,
                            iso3 = lv1.Attribute("iso3").Value
                        }).ToList();

            return lv1s;
        }


        public List<SocietyTerittory> GetSocietyTerittory()
        {

            var xmlfilepath = Path.Combine(path, @"TracksRelated\society_territory.xml");
            XDocument xdoc = XDocument.Load(xmlfilepath);
            var lv1s = (from lv1 in xdoc.Descendants("RECORD")
                        select new SocietyTerittory
                        {
                            FkCountryId = (int)lv1.Attribute("FkCountryId"),
                            FkTerritoryId = (int)lv1.Attribute("FkTerritoryId"),
                            Society_Id = (int)lv1.Attribute("Society_Id"),
                        }).ToList();

            return lv1s;
        }


        public List<Publishers> GetPublishers()
        {

            var xmlfilepath = Path.Combine(path, @"TracksRelated\publisher.xml");
            XDocument xdoc = XDocument.Load(xmlfilepath);
            var lv1s = (from lv1 in xdoc.Descendants("RECORD")
                        select new Publishers
                        {
                            CAE_Number = RandomNumber().ToString(),
                            Publisher_ID = (int)lv1.Attribute("Publisher_ID"),
                            PublisherMRSociety = string.IsNullOrEmpty(lv1.Attribute("PublisherMRSociety").Value) ? 0 : (int)lv1.Attribute("PublisherMRSociety"),
                            PublisherPRSociety = string.IsNullOrEmpty(lv1.Attribute("PublisherPRSociety").Value) ? 0 : (int)lv1.Attribute("PublisherPRSociety"),
                            Publisher_Name = lv1.Attribute("Publisher_Name").Value,
                        }).ToList();

            return lv1s;           
        }


        public List<CatalogPublishers> GetCatalogPublishers()
        {

            var xmlfilepath = Path.Combine(path, @"TracksRelated\catalogpublishers.xml");
            XDocument xdoc = XDocument.Load(xmlfilepath);
            var lv1s = (from lv1 in xdoc.Descendants("RECORD")
                        select new CatalogPublishers
                        {
                            fkPublisherID = (int)lv1.Attribute("fkPublisherID"),
                            CatalogPublihserID = (int)lv1.Attribute("ID"),
                            fkCatalogID = (int)lv1.Attribute("fkCatalogID"),
                        }).ToList();

            return lv1s;       
        }


        public List<Tracks> GetTracks()
        {
            var lstTracks = new List<Tracks>();

            for (int i = 1; i < 30; i++)
            {
                var index = random.Next(1, 5);
                var tracks = new Tracks
                {
                    ArtistID = index,
                    DHours = 0,
                    DMinutes = 4,
                    DSeconds = 2,
                    ISWCNumber = "56743234",
                    ReleaseDate = DateTime.Now,
                    SongTitle = $"Song No{i} of Artist{index}",
                    TrackID = i,
                    NewTrackID = i,
                    CatalogueNumber = i.ToString()
                };
                lstTracks.Add(tracks);
            }

            return lstTracks;
        }

        public List<TrackRegistration> GetTrackRegistration()
        {
            var listtrackReg = new List<TrackRegistration>();
            var tracks = GetTracks().Select(aa => aa.TrackID).Distinct();
            var index = 1;
            var random = new Random();
            var randomTunecodes = new List<string> {"876764DG", "1298376H", "8564567Y", "", "", "AS87670543", "", "19287HJ7",
                "HY787678", "12KI8987", "","K17283746L","A768276P","","1987653H","1298764H"};
            //registered tracks for first five societies
            foreach (var trackid in tracks)
            {
                for (int i = 1; i < 5; i++)
                {
                    var randIndex = random.Next(0, randomTunecodes.Count);
                    var trackreg = new TrackRegistration
                    {
                        CreatedDate = DateTime.Now,
                        SocietyID = i,
                        TrackID = trackid,
                        TrackRegistrationID = index,
                        Tunecode = randomTunecodes[randIndex]
                    };
                    listtrackReg.Add(trackreg);
                    ++index;
                }
            }

            return listtrackReg;
        }

        public List<Writers> GetWriters()
        {
            var getArtist = GetArtists().Select(aa => aa.ArtistID);
            var index = 1;
            var listWriters = new List<Writers>();
            foreach (var aid in getArtist)
            {
                for (int i = 1; i < 5; i++)
                {
                    var writ = new Writers
                    {
                        CAENo = "23423432343",
                        FirstName = $"Writer{index} ",
                        SurName = $"khan",
                        fkArtistID = aid,
                        ISMemberOfMCPS = true,
                        ISMemberOfPRS = true,
                        WriterId = index
                    };
                    listWriters.Add(writ);
                    ++index;
                    var writ2 = new Writers
                    {
                        CAENo = "",
                        FirstName = $"Writer{index} ",
                        SurName = $"khan",
                        fkArtistID = aid,
                        ISMemberOfMCPS = true,
                        ISMemberOfPRS = true,
                        WriterId = index
                    };
                    listWriters.Add(writ2);
                    ++index;
                }
            }
            return listWriters;
        }


        public List<WriterRegistration> GetWriterRegistration()
        {
            var listWritReg = new List<WriterRegistration>();
            var writerIds = GetWriters().Select(aa => aa.WriterId).ToList();
            var random = new Random();
            for (int i = 1; i < 5; i++)
            {
                var randomindex = random.Next(0, writerIds.Count);
                var writerReg = new WriterRegistration
                {
                    WriterRegistrationID = i,
                    PROSocietyID = i,
                    WriterID = writerIds[randomindex]
                };
                listWritReg.Add(writerReg);
            }
            return listWritReg;
        }

        public List<TrackWriter> GetTrackWriters()
        {
            var listtrackWriters = new List<TrackWriter>();
            var tracks = GetTracks();
            var writers = GetWriters();
            var artistIds = GetArtists().Select(sa => sa.ArtistID);
            var random = new Random();
            var index = 1;
            foreach (var aid in artistIds)
            {
                var artistTracks = tracks.Where(ss => ss.ArtistID == aid);
                var artistsWriters = writers.Where(sa => sa.fkArtistID == aid);
                var artistWriterids = artistsWriters.Select(aa => aa.WriterId).ToList();
                var tracksids = artistTracks.Select(aa => aa.TrackID);
                foreach (var tid in tracksids)
                {
                    if (tid % 2 == 0)
                    {
                        var randIndex = random.Next(1, artistWriterids.Count);
                        var trackWriter = new TrackWriter
                        {
                            TrackWriterID = index,
                            TrackID = tid,
                            WriterID = artistWriterids[randIndex],
                            ComposerCapacity_WriterRole = "Music/ Lyric Writer",
                            ComposerControlled_IsRightToCollect = true,
                            SongShare = 100,
                            PublisherControlled = true,
                            IsRightToCollect = true

                        };
                        ++index;
                        listtrackWriters.Add(trackWriter);
                    }
                    else
                    {
                        var randIndex = random.Next(1, artistWriterids.Count);
                        var trackWriter1 = new TrackWriter
                        {
                            TrackWriterID = index,
                            TrackID = tid,
                            WriterID = artistWriterids[randIndex],
                            ComposerCapacity_WriterRole = "Music/ Lyric Writer",
                            ComposerControlled_IsRightToCollect = true,
                            SongShare = 50,
                            PublisherControlled = true,
                            IsRightToCollect = true
                        };
                        ++index;
                        listtrackWriters.Add(trackWriter1);
                        randIndex = random.Next(1, artistWriterids.Count);
                        var trackWriter2 = new TrackWriter
                        {
                            TrackWriterID = index,
                            TrackID = tid,
                            WriterID = artistWriterids[randIndex],
                            ComposerCapacity_WriterRole = "Music/ Lyric Writer",
                            ComposerControlled_IsRightToCollect = true,
                            SongShare = 50,
                            PublisherControlled = true,
                            IsRightToCollect = true
                        };
                        ++index;
                        listtrackWriters.Add(trackWriter2);
                    }
                }
            }
            return listtrackWriters;
        }




        #region CWR Tables

        public List<cwrpublisher> GetcwrPublisher()
        {
            var xmlfilderPath = Path.Combine(path, "cwrpublisher.xml");
            XDocument xdoc = XDocument.Load(xmlfilderPath);

            var lv1s = (from lv1 in xdoc.Descendants("RECORD")
                        select new cwrpublisher
                        {
                            AgreementType = lv1.Element("AgreementType").Value,
                            Filler = lv1.Element("Filler").Value,
                            Id = (int)lv1.Element("Id"),
                            InterestedParty = lv1.Element("InterestedParty").Value,
                            ISAC = lv1.Element("ISAC").Value,
                            MROwnershipShare = lv1.Element("MROwnershipShare").Value,
                            MRSocietyId = lv1.Element("MRSocietyId").Value,
                            PROwnershipShare = lv1.Element("PROwnershipShare").Value,
                            PRSocietyId = lv1.Element("PRSocietyId").Value,
                            PublisherCAE = lv1.Element("PublisherCAE").Value,
                            PublisherIPI = lv1.Element("PublisherIPI").Value,
                            PublisherName = lv1.Element("PublisherName").Value,
                            PublisherSequence = lv1.Element("PublisherSequence").Value,
                            PublisherType = lv1.Element("PublisherType").Value,
                            RecordPrefix = lv1.Element("RecordPrefix").Value,
                            RecordSequence = lv1.Element("RecordSequence").Value,
                            Refusal = lv1.Element("Refusal").Value,
                            SocietyAssignedAgreementNumber = lv1.Element("SocietyAssignedAgreementNumber").Value,
                            SpecialAgreements = lv1.Element("SpecialAgreements").Value,
                            SROwnershipShare = lv1.Element("SROwnershipShare").Value,
                            SRSocietyId = lv1.Element("SRSocietyId").Value,
                            SubmitterAgreementNumber = lv1.Element("SubmitterAgreementNumber").Value,
                            Tax_ID = lv1.Element("Tax_ID").Value,
                            TransactionSequence = lv1.Element("TransactionSequence").Value,
                            UnknownPublisher = lv1.Element("UnknownPublisher").Value,
                            USALicense = lv1.Element("USALicense").Value,
                        }).ToList();


            return lv1s;
        }


        public List<cwrterritory> GetcwrTerritory()
        {
            var xmlfilepath = Path.Combine(path, "cwrterritory.xml");

            XDocument xdoc = XDocument.Load(xmlfilepath);
            var lv1s = (from lv1 in xdoc.Descendants("RECORD")
                        select new cwrterritory
                        {
                            Constant = lv1.Element("Constant").Value,
                            Inclusion_Exclusion = lv1.Element("Inclusion_Exclusion").Value,
                            Id = (int)lv1.Element("Id"),
                            InterestedParty = lv1.Element("InterestedParty").Value,
                            MRCollectionShare = lv1.Element("MRCollectionShare").Value,
                            PRCollectionShare = lv1.Element("PRCollectionShare").Value,
                            Sequence = lv1.Element("Sequence").Value,
                            SharesChange = lv1.Element("SharesChange").Value,
                            SRCollectionShare = lv1.Element("SRCollectionShare").Value,
                            TerritoryCode = lv1.Element("TerritoryCode").Value,
                            RecordPrefix = lv1.Element("RecordPrefix").Value,
                            RecordSequence = lv1.Element("RecordSequence").Value,
                            TransactionSequence = lv1.Element("TransactionSequence").Value,
                        }).ToList();

            return lv1s;
        }

        public List<cwrwriterterritory> Getcwrwriterterritory()
        {
            var xmlfilderPath = Path.Combine(path, "cwrwriterterritory.xml");
            XDocument xdoc = XDocument.Load(xmlfilderPath);
            var lv1s = (from lv1 in xdoc.Descendants("RECORD")
                        select new cwrwriterterritory
                        {
                            TransactionSequence = lv1.Element("TransactionSequence").Value,
                            Inclusion_Exclusion = lv1.Element("Inclusion_Exclusion").Value,
                            Id = (int)lv1.Element("Id"),
                            InterestedParty = lv1.Element("InterestedParty").Value,
                            MRCollectionShare = lv1.Element("MRCollectionShare").Value,
                            PRCollectionShare = lv1.Element("PRCollectionShare").Value,
                            Sequence = lv1.Element("Sequence").Value,
                            SharesChange = lv1.Element("SharesChange").Value,
                            SRCollectionShare = lv1.Element("SRCollectionShare").Value,
                            TerritoryCode = lv1.Element("TerritoryCode").Value,
                            RecordPrefix = lv1.Element("RecordPrefix").Value,
                            RecordSequence = lv1.Element("RecordSequence").Value
                        }).ToList();

            return lv1s;
        }

        public List<cwrwriter> Getcwrwriter()
        {
            var xmlfilderPath = Path.Combine(path, "cwrwriter.xml");
            XDocument xdoc = XDocument.Load(xmlfilderPath);
            var lv1s = (from lv1 in xdoc.Descendants("RECORD")
                        select new cwrwriter
                        {
                            Filler = lv1.Element("Filler").Value,
                            MRShare = lv1.Element("MRShare").Value,
                            Id = (int)lv1.Element("Id"),
                            InterestedParty = lv1.Element("InterestedParty").Value,
                            MRSociety = lv1.Element("MRSociety").Value,
                            PersonalNumber = lv1.Element("PersonalNumber").Value,
                            PRShare = lv1.Element("PRShare").Value,
                            PRSociety = lv1.Element("PRSociety").Value,
                            RecordPrefix = lv1.Element("RecordPrefix").Value,
                            RecordSequence = lv1.Element("RecordSequence").Value,
                            Refusal = lv1.Element("Refusal").Value,
                            Reversionary = lv1.Element("Reversionary").Value,
                            SRShare = lv1.Element("SRShare").Value,
                            SRSociety = lv1.Element("SRSociety").Value,
                            TaxID = lv1.Element("TaxID").Value,
                            TransactionSequence = lv1.Element("TransactionSequence").Value,
                            USALicense = lv1.Element("USALicense").Value,
                            WorkHire = lv1.Element("WorkHire").Value,
                            WriterCAE = lv1.Element("WriterCAE").Value,
                            WriterDesignationCode = lv1.Element("WriterDesignationCode").Value,
                            WriterFirstName = lv1.Element("WriterFirstName").Value,
                            WriterIPI = lv1.Element("WriterIPI").Value,
                            WriterLastName = lv1.Element("WriterLastName").Value,
                            WriterUnknown = lv1.Element("WriterUnknown").Value,
                        }).ToList();

            return lv1s;
        }

        public List<cwrwriterpublisher> Getcwrwriterpublisher()
        {
            var xmlfilderPath = Path.Combine(path, "cwrwriterpublisher.xml");
            XDocument xdoc = XDocument.Load(xmlfilderPath);
            var lv1s = (from lv1 in xdoc.Descendants("RECORD")
                        select new cwrwriterpublisher
                        {
                            PublisherIP = lv1.Element("PublisherIP").Value,
                            PublisherName = lv1.Element("PublisherName").Value,
                            Id = (int)lv1.Element("Id"),
                            SocietyAssignedAgreementNumber = lv1.Element("SocietyAssignedAgreementNumber").Value,
                            SubmitterAgreementNumber = lv1.Element("SubmitterAgreementNumber").Value,
                            WriterIP = lv1.Element("WriterIP").Value,
                            RecordSequence = lv1.Element("RecordSequence").Value,
                            RecordPrefix = lv1.Element("RecordPrefix").Value,
                            TransactionSequence = lv1.Element("TransactionSequence").Value,

                        }).ToList();

            return lv1s;
        }

        public List<cwrrecordformat> Getcwrrecordformat()
        {
            var xmlfilderPath = Path.Combine(path, "cwrrecordformat.xml");
            XDocument xdoc = XDocument.Load(xmlfilderPath);
            var lv1s = (from lv1 in xdoc.Descendants("RECORD")
                        select new cwrrecordformat
                        {
                            CatalogueNumber = lv1.Element("CatalogueNumber").Value,
                            CompositeComponentCount = lv1.Element("CompositeComponentCount").Value,
                            Id = (int)lv1.Element("Id"),
                            CompositeType = lv1.Element("CompositeType").Value,
                            ContactId = lv1.Element("ContactId").Value,
                            ContactName = lv1.Element("ContactName").Value,
                            CopyrightDate = lv1.Element("CopyrightDate").Value,
                            CopyrightNumber = lv1.Element("CopyrightNumber").Value,
                            DistributionCategory = lv1.Element("DistributionCategory").Value,
                            Duration = lv1.Element("Duration").Value,
                            ExceptionalClause = lv1.Element("ExceptionalClause").Value,
                            ExcerptType = lv1.Element("ExcerptType").Value,
                            GrandRights = lv1.Element("GrandRights").Value,
                            ISWC = lv1.Element("ISWC").Value,
                            LanguageCode = lv1.Element("LanguageCode").Value,
                            LyricAdaption = lv1.Element("LyricAdaption").Value,
                            MusicArrangement = lv1.Element("MusicArrangement").Value,
                            OpusNumber = lv1.Element("OpusNumber").Value,
                            PrintPublicationDate = lv1.Element("PrintPublicationDate").Value,
                            PriorityFlag = lv1.Element("PriorityFlag").Value,
                            Recorded = lv1.Element("Recorded").Value,
                            RecordPrefix = lv1.Element("RecordPrefix").Value,
                            RecordSequence = lv1.Element("RecordSequence").Value,
                            TextMusicRelationship = lv1.Element("TextMusicRelationship").Value,
                            TransactionSequence = lv1.Element("TransactionSequence").Value,
                            VersionType = lv1.Element("VersionType").Value,
                            WorkId = lv1.Element("WorkId").Value,
                            WorkTitle = lv1.Element("WorkTitle").Value,
                            WorkType = lv1.Element("WorkType").Value,

                        }).ToList();

            return lv1s;
        }

        public List<cwrtemplate> Getcwrtemplates()
        {
            var xmlfilderPath = Path.Combine(path, "cwrtemplates.xml");
            var groupheaderPath = Path.Combine(path, "groupheaders.xml");
            var grouptrailerPath = Path.Combine(path, "grouptrailers.xml");
            var headerPath = Path.Combine(path, "headers.xml");
            var trailerPath = Path.Combine(path, "trailers.xml");

            XDocument xdoc = XDocument.Load(xmlfilderPath);
            XDocument xdocgroupheader = XDocument.Load(groupheaderPath);
            XDocument xdocgrouptrailor = XDocument.Load(grouptrailerPath);
            XDocument xdocheader = XDocument.Load(headerPath);
            XDocument xdoctrailor = XDocument.Load(trailerPath);

            var groupheaders = (from lv1 in xdocgroupheader.Descendants("RECORD")
                                select new groupheader
                                {
                                    BatchRequest = lv1.Element("BatchRequest").Value,
                                    Id = (int)lv1.Element("Id"),
                                    GroupId = lv1.Element("GroupId").Value,
                                    RecordType = lv1.Element("RecordType").Value,
                                    SubmissionType = lv1.Element("SubmissionType").Value,
                                    TransactionType = lv1.Element("TransactionType").Value,
                                    VersionNumber = lv1.Element("VersionNumber").Value,

                                });

            var grouptailors = (from lv1 in xdocgrouptrailor.Descendants("RECORD")
                                select new grouptrailer
                                {
                                    RecordCount = lv1.Element("RecordCount").Value,
                                    Id = (int)lv1.Element("Id"),
                                    GroupId = lv1.Element("GroupId").Value,
                                    RecordType = lv1.Element("RecordType").Value,
                                    TransactionCount = lv1.Element("TransactionCount").Value,
                                });

            var headers = (from lv1 in xdocheader.Descendants("RECORD")
                           select new header
                           {
                               CreationDate = lv1.Element("CreationDate").Value,
                               Id = (int)lv1.Element("Id"),
                               CreationTime = lv1.Element("CreationTime").Value,
                               RecordType = lv1.Element("RecordType").Value,
                               EDINumber = lv1.Element("EDINumber").Value,
                               SenderId = lv1.Element("SenderId").Value,
                               SenderName = lv1.Element("SenderName").Value,
                               SenderType = lv1.Element("SenderType").Value,
                               TransmissionDate = lv1.Element("TransmissionDate").Value,
                           });



            var trailors = (from lv1 in xdoctrailor.Descendants("RECORD")
                            select new trailer
                            {
                                GroupCount = lv1.Element("GroupCount").Value,
                                Id = (int)lv1.Element("Id"),
                                RecordCount = lv1.Element("RecordCount").Value,
                                RecordType = lv1.Element("RecordType").Value,
                                TransactionCount = lv1.Element("TransactionCount").Value,
                            });




            var lv1s = (from lv1 in xdoc.Descendants("RECORD")
                        select new cwrtemplate
                        {
                            groupheader = groupheaders.FirstOrDefault(aa => aa.Id == (int)lv1.Element("GroupHeaderId")),
                            GroupHeaderId = (int)lv1.Element("GroupHeaderId"),
                            Id = (int)lv1.Element("Id"),
                            grouptrailer = grouptailors.FirstOrDefault(aa => aa.Id == (int)lv1.Element("GroupTrailerId")),
                            GroupTrailerId = (int)lv1.Element("GroupTrailerId"),
                            header = headers.FirstOrDefault(aa => aa.Id == (int)lv1.Element("HeaderId")),
                            HeaderId = (int)lv1.Element("HeaderId"),
                            trailer = trailors.FirstOrDefault(aa => aa.Id == (int)lv1.Element("TrailerId")),
                            TrailerId = (int)lv1.Element("TrailerId"),

                        }).ToList();

            return lv1s;
        }

        #endregion


        #region Private Methods

        public int RandomNumber()
        {
            int min = 111111111;
           int max = 999999999;
            Random random = new Random();
            return random.Next(min, max);
        }

        private static Random random = new Random();
        public static string RandomString()
        {
            var lengtharray = new List<int> { 0,5,6,0,7,8,0,9,0,10,0,11,0,12,10, };
            var randome2 = new Random();
            var randomIndex = randome2.Next(0, lengtharray.Count);
            var randomStringLength = lengtharray[randomIndex];
            if (randomStringLength == 0)
            {
                return "";
            }
            else
            {
                const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                return new string(Enumerable.Repeat(chars, randomStringLength)
                  .Select(s => s[random.Next(s.Length)]).ToArray());
            }
        }
        #endregion

    }
}
