﻿using CWR_App.Models.CWRModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CWR_App.HelperMethods
{
   public class CWRFormatter
    {
        public string getHeaderString(header hdr)
        {
            string hdrString = hdr.RecordType +
                hdr.SenderType +
                hdr.SenderId +
                hdr.SenderName +
                hdr.EDINumber +
                hdr.CreationDate +
                hdr.CreationTime +
                hdr.TransmissionDate;

            return hdrString;
        }

        public string getGroupHeaderString(groupheader grh)
        {
            string grhString = grh.RecordType +
               grh.TransactionType +
               grh.GroupId +
               grh.VersionNumber +
               grh.BatchRequest;

            return grhString;
        }

        public string getGroupTrailerString(grouptrailer grt)
        {
            string grtString = grt.RecordType +
                grt.GroupId +
                grt.TransactionCount +
                grt.RecordCount;

            return grtString;
        }

        public string getRowFormatString(Object rf)
        {
            return rf.GetType().GetProperties().Where(prop => !prop.Name.Equals("Id")).Aggregate("", (current, prop) => current + prop.GetValue(rf, null));
        }

        public string getTrailerString(trailer trl)
        {
            string trlString = trl.RecordType +
                trl.GroupCount +
                trl.TransactionCount +
                trl.RecordCount;

            return trlString;
        }

        public string formatString(string value, string dbValue)
        {
            // Max length
            value = Truncate(value, dbValue.Length);

            return value.ToUpperInvariant() + dbValue.Substring(0, dbValue.Length - value.Length);
        }

        public string formatNumeric(string value, string dbValue)
        {
            // Max length
            value = Truncate(value, dbValue.Length);

            return dbValue.Substring(0, dbValue.Length - value.Length) + value.ToUpperInvariant();
        }

        public string formatPercentage(string value)
        {
            string[] split = value.Split('.');
            string whole = split[0].PadLeft(3, '0');
            string dec = split.Count() > 1 ? Truncate(split[1].PadRight(2, '0'), 2) : "00";
            string result = Truncate(whole + dec, 5);

            return result;
        }

        private string Truncate(string source, int length)
        {
            if (source.Length > length)
            {
                source = source.Substring(0, length);
            }
            return source;
        }
    }
}
