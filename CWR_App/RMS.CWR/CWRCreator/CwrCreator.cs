﻿using CWR_App.AppDbContext;
using CWR_App.HelperMethods;
using CWR_App.Models.CWRModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace CWR_App.CWRCreator
{
    public class CwrCreator
    {

        private readonly DbContext dbEntity = new DbContext();
        private readonly CWRFormatter _cf = new CWRFormatter();

        // DB Entities
        private cwrtemplate _cwrtemplate;
        private cwrrecordformat _rev;
        private cwrpublisher _publisher;
        private cwrpublisher _subPublisher;
        private cwrterritory _territory;
        private cwrwriter _writer;
        private cwrwriterpublisher _writerpublisher;
        private cwrwriterterritory _writerterritory;

        // Counters
        private int _transactions;
        private int _sequence;
        private int _pubsequence;
        private int _records;
        private int _totalRecords;

        // Shares
        double _comPoShare;
        double _comMoShare;


        private readonly Dictionary<string, double> _totalPrShare = new Dictionary<string, double>();
        private readonly Dictionary<string, double> _totalMrShare = new Dictionary<string, double>();

        // Misc 
        private StringBuilder _workrows;
        private string _societyPerformingCode;
        private string _agreementNumber;
        private string _territoryIp;
        private bool _overrideControlled;

        public string Xml { get; set; }

        public Dictionary<string, string> Post(string xml, string physicalPath)
        {


            var locations = new Dictionary<string, string>();

            // Get schedule from request and parse as XML  
            try
            {
                //The service generates a cwr file based on the xml file it receives in this section.
                Xml = xml;
                XElement schedule;
                schedule = XElement.Parse(Xml);


                //Generate a cwr for all societies
                var societies = from socs in schedule.Elements("Societies")
                                from soc in socs.Elements("Society")
                                select soc;


                var query = from r in schedule.Descendants("Society")
                            select r;

                var firstOrDefault = query.FirstOrDefault();


                if (firstOrDefault != null)
                {

                    string versionNumber = firstOrDefault.Attribute("VersionNumber").Value;
                    foreach (var society in societies)
                    {
                        _totalRecords = 0;
                        _transactions = 0;

                        //Load template and set header and footer
                        _cwrtemplate = GetCwrTemplate(1); // template id = 1 is the deafult cwr template we can add more template as per our requirments
                        SetCwrHeader();

                        //Set global variables
                        _agreementNumber = society.Attribute("AgreementNumber").Value;
                        _societyPerformingCode = society.Attribute("PerformingCode").Value;


                        // Build arrayOfWorks
                        List<WorkDetails> arrayOfWorks;
                        arrayOfWorks = GetArrayOfWorks(schedule, society);

                        //set footer
                        SetCwrFooter();

                        var cwrStringBuilder = new StringBuilder();
                        cwrStringBuilder.Append(_cf.getHeaderString(_cwrtemplate.header));
                        cwrStringBuilder.Append("\r\n");
                        cwrStringBuilder.Append(_cf.getGroupHeaderString(_cwrtemplate.groupheader));
                        cwrStringBuilder.Append("\r\n");
                        foreach (WorkDetails workDetails in arrayOfWorks)
                        {
                            cwrStringBuilder.Append(workDetails.WorkLine);
                        }
                        cwrStringBuilder.Append(_cf.getGroupTrailerString(_cwrtemplate.grouptrailer));
                        cwrStringBuilder.Append("\r\n");
                        cwrStringBuilder.Append(_cf.getTrailerString(_cwrtemplate.trailer));

                        //Dont store files in S3 if the method is under test
                        string cwrString = cwrStringBuilder.ToString();
                        if (true)
                        {
                            if (arrayOfWorks.Count > 0)
                            {
                                string xmlname = "CW" +
                                                 DateTime.Now.ToString("yy") +
                                                 versionNumber +
                                                 schedule.Attribute("SenderId").Value +
                                                 "_" +
                                                 society.Attribute("PerformingCode").Value + ".txt";


                                using (var sr = new StreamWriter(physicalPath + xmlname, true))
                                {
                                    sr.Write(cwrString);
                                }

                                locations.Add("path", physicalPath + xmlname);
                            }
                        }
                        else
                        {
                            // StoreFilesInS3
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return locations;
            }
            return locations;
        }



        #region Helper methods

        private List<WorkDetails> GetArrayOfWorks(XElement schedule, XElement society)
        {
            double _pubPoShare;
            double _pubMoShare;


            var arrayOfWorks = new List<WorkDetails>();


            _workrows = new StringBuilder();


            IEnumerable<XElement> works = schedule.Elements("Work");
            #region DBCalls
            // set which row to pick up from DB for subpublisher 
            int subpub = 0;

            if (_societyPerformingCode.Equals("021"))
            {
                subpub = 3;
            }
            else if (_societyPerformingCode.Equals("010"))
            {
                subpub = 4;
            }
            else if (_societyPerformingCode.Equals("101"))
            {
                subpub = 6;
            }
            else if (_societyPerformingCode.Equals("071"))
            {
                subpub = 16;
            }
            else
            {
                subpub = 2;
            }
            // get template rows from DB
            try
            {
                _rev = Getcwrrecordformat(2);
                _publisher = Getcwrpublisher(1);
                _territory = Getcwrterritory(1);
                _writer = Getcwrwriter(1);
                _writerpublisher = Getcwrwriterpublisher(1);
                _writerterritory = Getcwrwriterterritory(1);
                _subPublisher = subpub > 0 ? Getcwrpublisher(subpub) : null;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            #endregion  
            // Build works
            foreach (var work in works)
            {

                _records = 0;
                _pubsequence = 1;
                _totalPrShare.Clear();
                _totalMrShare.Clear();

                // Calculate shares for SPU/OPU rows 

                // Check for exclusions
                var exclusion = from exclusions in work.Elements("Exclusions")
                                from exclude in exclusions.Elements("Exclude")
                                where exclude.Attribute("PerformingCode").Value.Equals(_societyPerformingCode)
                                select exclude;

                // Is that correct? All following steps are skipt if there is an exclusion.
                if (!exclusion.Any())
                {
                    var comps = from coms in work.Elements("Composers")
                                from com in coms.Elements("Composer")
                                select com;
                    #region CalculateShares
                    foreach (var cmp in comps)
                    {
                        // Do ASCAP/BMI/SESAC check
                        _overrideControlled = false;

                        //This handles an edge case with US societies. Override the standard behaviour if the file is generated for one of these societies
                        //and writers are rigistered with certain societies.
                        //Check if the composer is controlled by us
                        if (cmp.Attribute("ComposerControlled").Value.Equals("Y"))
                        {
                            //Check if the file is generated for ASCAP
                            if (_societyPerformingCode.Equals("010")) //ASCAP
                            {
                                //Check if the composer is not registered with ASCAP
                                if (!cmp.Attribute("ComposerPRSociety").Value.Equals("010"))
                                {
                                    _overrideControlled = true;
                                }
                            }
                            //Check if the file is generated for BMI
                            else if (_societyPerformingCode.Equals("021")) // BMI
                            {
                                //Check if the composer is registered with ASCAP or SESAC
                                if (cmp.Attribute("ComposerPRSociety").Value.Equals("010") || cmp.Attribute("ComposerPRSociety").Value.Equals("071"))
                                {
                                    _overrideControlled = true;
                                }
                            }
                            //Check if the file is generated for SESAC
                            else if (_societyPerformingCode.Equals("071") &&
                                     !cmp.Attribute("ComposerPRSociety").Value.Equals("071"))
                            {
                                _overrideControlled = true;
                            }
                        }

                        var originalCae = "";
                        var publishers = from pc in cmp.Elements("PublisherChain")
                                         from pub in pc.Elements("Publisher")
                                         select pub;
                        // refactor. Why do we use a loop and dont jump directly to the last element.

                        var publishersList = publishers as IList<XElement> ?? publishers.ToList();
                        foreach (var pub in publishersList)
                        {
                            originalCae = pub.Attribute("PublisherCAE").Value;
                        }

                        // Calculate publisher and composer share.
                        decimal publisherPrCollection = 0.5m;
                        decimal publisherMrCollection = 1.0m;

                        // we do we do this test?
                        //Check if the file is for the PRS 
                        if ((_societyPerformingCode.Equals("052") && originalCae.Equals("691477506")))
                        {
                            publisherPrCollection = 0.2m;

                            decimal moShare = Convert.ToDecimal(cmp.Attribute("ComposerMCPSShare").Value);
                            if (cmp.Attribute("ComposerMRSociety").Value.Equals("044") && moShare == (decimal)0.0)
                            {

                                publisherMrCollection = 0.2m;
                            }


                        }


                        decimal composerPrCollection = 1 - publisherPrCollection;
                        decimal composerMrCollection = 1 - publisherMrCollection;

                        decimal songShare = decimal.Parse(cmp.Attribute("SongShare").Value);

                        if (cmp.Attribute("ComposerControlled").Value.Equals("Y") && !_overrideControlled)
                        {
                            decimal comPoShare = Math.Round((songShare * composerPrCollection), 2);
                            decimal comMoShare = Math.Round((songShare * composerMrCollection), 2);

                            cmp.SetAttributeValue("ComposerPOShare", comPoShare.ToString());

                            cmp.SetAttributeValue("ComposerMOShare", comMoShare.ToString());
                        }
                        else
                        {
                            cmp.SetAttributeValue("ComposerPOShare", songShare);
                            cmp.SetAttributeValue("ComposerMOShare", songShare);
                        }


                        foreach (var pub in publishersList)
                        {
                            //  this should be in the xml!?
                            pub.SetAttributeValue("PublisherCapacity", "E");

                            decimal pubPoShare = Math.Round((songShare * publisherPrCollection), 2);
                            decimal pubMoShare = Math.Round((songShare * publisherMrCollection), 2);

                            pub.SetAttributeValue("PublisherPOShare", pubPoShare.ToString(CultureInfo.InvariantCulture));

                            pub.SetAttributeValue("PublisherMOShare", pubMoShare.ToString(CultureInfo.InvariantCulture));

                            //Check if the composer is controlled and if we dont override the behaviour
                            if (cmp.Attribute("ComposerControlled").Value.Equals("Y") && !_overrideControlled )
                            {
                                //Get publisher share for performances and mechanicals
                                double pshare = FormatShare(pub.Attribute("PublisherPOShare").Value);
                                double mshare = FormatShare(pub.Attribute("PublisherMOShare").Value);

                                //If we already did calculate a publisher share add the new value to the existing one
                                if (_totalPrShare.ContainsKey(pub.Attribute("PublisherId").Value))
                                {
                                    _totalPrShare[pub.Attribute("PublisherId").Value] = _totalPrShare[pub.Attribute("PublisherId").Value] + pshare;
                                }
                                //Else create a new entry for the publisher's share
                                else
                                {
                                    _totalPrShare.Add(pub.Attribute("PublisherId").Value, pshare);
                                }

                                //If we already did calculate a publisher share add the new value to the existing one
                                if (_totalMrShare.ContainsKey(pub.Attribute("PublisherId").Value))
                                {
                                    _totalMrShare[pub.Attribute("PublisherId").Value] = _totalMrShare[pub.Attribute("PublisherId").Value] + mshare;
                                }
                                //Else create a new entry for the publisher's share
                                else
                                {
                                    _totalMrShare.Add(pub.Attribute("PublisherId").Value, mshare);
                                }
                            }
                            //Execute this if the cwr file is for a US society and the writer is not registered with that society.
                            else
                            {
                                if (!_totalPrShare.ContainsKey(pub.Attribute("PublisherId").Value)) _totalPrShare.Add(pub.Attribute("PublisherId").Value, 0);
                                if (!_totalMrShare.ContainsKey(pub.Attribute("PublisherId").Value)) _totalMrShare.Add(pub.Attribute("PublisherId").Value, 0);
                            }
                        }

                    }
                    #endregion
                    // Build REV row
                    _workrows = new StringBuilder();
                    _workrows.Append(_cf.getRowFormatString(GetRev(_rev, work)) + "\r\n");
                    _records++;

                    // get XML children           
                    #region PublisherInfo
                    var composers = from coms in work.Elements("Composers")
                                    from com in coms.Elements("Composer")
                                    select com;

                    var territories = from sts in society.Elements("Territories")
                                      from st in sts.Elements("Territory")
                                      select st;

                    var dupecheck = new List<string>();


                    // Iterate composers and get publisher rows 
                    var composersList = composers as IList<XElement> ?? composers.ToList();
                    var territoriesList = territories as IList<XElement> ?? territories.ToList();
                    foreach (var composer in composersList)
                    {
                        var publishers = from pc in composer.Elements("PublisherChain")
                                         from pub in pc.Elements("Publisher")
                                         select pub;

                        // Check for duplicates & controlled first

                        var publishersList = publishers as IList<XElement> ?? publishers.ToList();
                        foreach (var spu in publishersList)
                        {
                            if (spu.Attribute("PublisherControlled").Value.Equals("Y"))
                            {
                                var last = publishersList.Last();

                                //if not duplicate
                                if (!dupecheck.Contains(spu.Attribute("PublisherId").Value))
                                {
                                    //add publisher id to dupecheck
                                    dupecheck.Add(spu.Attribute("PublisherId").Value);

                                    // Build SPU 
                                    var firstSpuLine = _cf.getRowFormatString(GetSpu(_publisher, spu)) + "\r\n";
                                    _workrows.Append(firstSpuLine);
                                    _records++;

                                    _territoryIp = spu.Attribute("PublisherCAE").Value;

                                    // Publisher Shares 
                                    _pubPoShare = FormatShare(spu.Attribute("PublisherPOShare").Value);
                                    _pubMoShare = FormatShare(spu.Attribute("PublisherMOShare").Value);

                                    // if last in chain, inject sub-publisher
                                    if (spu.Equals(last) && _subPublisher != null)
                                    {
                                        if (spu.Attribute("PublisherCAE").Value.Equals("691477506") && _societyPerformingCode.Equals("052"))
                                        {
                                            _territoryIp = "509030978";
                                        }
                                        else
                                        {

                                            var secondSpuLine = _cf.getRowFormatString(GetSubPublisher(_subPublisher)) + "\r\n";
                                            _workrows.Append(secondSpuLine);
                                            _records++;

                                            _territoryIp = _subPublisher.PublisherCAE.TrimStart('0');
                                        }
                                    }

                                    // if collecting or last in chain...
                                    if ((spu.Attribute("PublisherControlled").Value.Equals("Y") || spu.Equals(last)) &&
                                        (_pubPoShare > 0 || _pubMoShare > 0))
                                    {
                                        _sequence = 1;

                                        foreach (var spt in territoriesList)
                                        {
                                            // Build SPT rows
                                            var sptLine = _cf.getRowFormatString(GetSpt(_territory, spu, spt)) +
                                                             "\r\n";
                                            _workrows.Append(sptLine);
                                            _sequence++;
                                            _records++;
                                        }
                                    }
                                }
                            }

                        }
                    }
                    _pubsequence++;
                    #endregion  
                    // Iterate composers and get writer rows 

                    var nonctrlcomposers = new List<XElement>();
                    var composerbuild = new StringBuilder();

                    //GEMA 035
                    //Checks for GEMA
                    #region GEMA OPULine Check

                    if (_societyPerformingCode.Equals("035")) // GEMA
                    {
                        bool addLine = false;
                        foreach (var composer in composersList)
                        {
                            // Build SWR 

                            if (composer.Attribute("ComposerControlled").Value.Equals("Y"))
                            {
                                //ignor
                            }
                            else
                            {
                                // build list of nonctrlcomposers
                                addLine = true;
                                break;
                            }
                        }
                        if (addLine)
                        {
                            const string opuLine = @"OPU{0}0000{1}01         COPYRIGHT CONTROL SHARES                     NE          00532837865              0990000009900000   00000";

                            _workrows.Append(string.Format(opuLine, _cf.formatNumeric(_transactions.ToString(), "00000000"), _cf.formatNumeric(_records.ToString(), "0000")) + "\r\n");
                            _records++;
                        }
                    }

                    #endregion
                    #region Composer Writer Controlled
                    foreach (var composer in composersList)
                    {
                        _comPoShare = FormatShare(composer.Attribute("ComposerPOShare").Value);
                        _comMoShare = FormatShare(composer.Attribute("ComposerMOShare").Value);

                        var publishers = from pc in composer.Elements("PublisherChain")
                                         from pub in pc.Elements("Publisher")
                                         select pub;


                        _overrideControlled = false;

                        if (_societyPerformingCode.Equals("010")) //ASCAP
                        {
                            if (!composer.Attribute("ComposerPRSociety").Value.Equals("010"))
                            {
                                _overrideControlled = true;
                            }
                        }
                        else if (_societyPerformingCode.Equals("021")) // BMI
                        {
                            if (composer.Attribute("ComposerPRSociety").Value.Equals("010") ||
                                composer.Attribute("ComposerPRSociety").Value.Equals("071"))
                            {
                                _overrideControlled = true;
                            }
                        }
                        else if (_societyPerformingCode.Equals("071") &&
                                 !composer.Attribute("ComposerPRSociety").Value.Equals("071"))
                        {
                            _overrideControlled = true;
                        }

                        // Build SWR 

                        if (composer.Attribute("ComposerControlled").Value.Equals("Y") &&
                            !_overrideControlled)
                        {

                            composerbuild.Append(_cf.getRowFormatString(GetSwr(_writer, composer)) + "\r\n");
                            _records++;

                            // if collecting...  

                            if (_comPoShare > 0 || _comMoShare > 0)
                            {
                                _sequence = 1;

                                foreach (var swt in territoriesList)
                                {
                                    // Build SWT rows
                                    var swtLine = _cf.getRowFormatString(GetSwt(_writerterritory, composer, swt)) + "\r\n";
                                    composerbuild.Append(swtLine);
                                    _sequence++;
                                    _records++;
                                }
                            }

                            foreach (var spu in publishers)
                            {
                                // if original publisher...

                                if (spu.Attribute("PublisherCapacity").Value.Equals("E"))
                                {
                                    // Build PWR row 
                                    composerbuild.Append(
                                        _cf.getRowFormatString(GetPwr(_writerpublisher, composer, spu)) + "\r\n");
                                    _records++;
                                }
                            }
                        }
                        else
                        {
                            // build list of nonctrlcomposers
                            nonctrlcomposers.Add(composer);
                        }
                    }


                    //Write Composers Line
                    _workrows.Append(composerbuild);
                    // Write OWR
                    foreach (var comp in nonctrlcomposers)
                    {
                        _workrows.Append(_cf.getRowFormatString(GetOwr(_writer, comp)) + "\r\n");
                        _records++;
                    }
                    #endregion


                    _transactions++;
                    _totalRecords = _totalRecords + _records;
                    var workDetails = new WorkDetails { WorkLine = _workrows.ToString(), WorkId = work.Attribute("Id").Value };
                    arrayOfWorks.Add(workDetails);
                }
            }
            return arrayOfWorks;
        }

        // generate a file for masstax and amendments songs    

        /// <summary>
        /// Sets values in the header of a cwr file
        /// </summary>
        private void SetCwrHeader()
        {
            // Set dynamic dates
            _cwrtemplate.header.CreationDate = DateTime.Now.ToString("yyyyMMdd");
            _cwrtemplate.header.CreationTime = DateTime.Now.ToString("HHmmss");
            // This should probably change depending on when sent???
            _cwrtemplate.header.TransmissionDate = DateTime.Now.ToString("yyyyMMdd");


        }

        /// <summary>
        /// Sets the total amount of records and transactions
        /// </summary>
        private void SetCwrFooter()
        {
            // Set total records & transactions
            _cwrtemplate.grouptrailer.TransactionCount = _cf.formatNumeric("00000000", _cwrtemplate.grouptrailer.TransactionCount);
            _cwrtemplate.trailer.TransactionCount = _cf.formatNumeric("00000000", _cwrtemplate.trailer.TransactionCount);
            _cwrtemplate.grouptrailer.RecordCount = _cf.formatNumeric("00000000", _cwrtemplate.grouptrailer.RecordCount);
            _cwrtemplate.trailer.RecordCount = _cf.formatNumeric("00000000", _cwrtemplate.trailer.RecordCount);

            _cwrtemplate.grouptrailer.TransactionCount = _cf.formatNumeric(_transactions.ToString(), _cwrtemplate.grouptrailer.TransactionCount);
            _cwrtemplate.trailer.TransactionCount = _cf.formatNumeric(_transactions.ToString(), _cwrtemplate.trailer.TransactionCount);
            _cwrtemplate.grouptrailer.RecordCount = _cf.formatNumeric((_totalRecords + 2).ToString(), _cwrtemplate.grouptrailer.RecordCount);
            _cwrtemplate.trailer.RecordCount = _cf.formatNumeric((_totalRecords + 4).ToString(), _cwrtemplate.trailer.RecordCount);
        }

        /// <summary>
        /// This method stores the resulting cwr files in Amazon's S3 service

        #region Publisher
        private cwrpublisher GetSpu(cwrpublisher cwrpublisher, XElement spu)
        {


            var rec = dbEntity.cwrpublisher.FirstOrDefault(aa => aa.Id == cwrpublisher.Id);
            var clonedSpu = rec;
            try
            {
                var tps = _totalPrShare.ContainsKey(spu.Attribute("PublisherId").Value) ? _totalPrShare[spu.Attribute("PublisherId").Value] : 0.00;
                var tms = _totalMrShare.ContainsKey(spu.Attribute("PublisherId").Value) ? _totalMrShare[spu.Attribute("PublisherId").Value] : 0.00;

                var pubName = spu.Attribute("PublisherName").Value;
                var pubCae = spu.Attribute("PublisherCAE").Value;

                clonedSpu.TransactionSequence = _cf.formatNumeric(_transactions.ToString(), cwrpublisher.TransactionSequence);
                clonedSpu.RecordSequence = _cf.formatNumeric(_records.ToString(), cwrpublisher.RecordSequence);
                clonedSpu.InterestedParty = _cf.formatNumeric(pubCae, cwrpublisher.InterestedParty);
                clonedSpu.PublisherName = _cf.formatString(pubName, cwrpublisher.PublisherName);
                clonedSpu.PublisherCAE = _cf.formatNumeric(pubCae, cwrpublisher.PublisherCAE);
                clonedSpu.PublisherType = _cf.formatString(spu.Attribute("PublisherCapacity").Value, cwrpublisher.PublisherType);
                clonedSpu.PRSocietyId = _cf.formatString(spu.Attribute("PublisherPRSociety").Value, cwrpublisher.PRSocietyId);
                if (string.IsNullOrEmpty(spu.Attribute("PublisherPRSociety").Value))
                    clonedSpu.PRSocietyId = _cf.formatString("099", cwrpublisher.PRSocietyId);

                clonedSpu.MRSocietyId = _cf.formatString(spu.Attribute("PublisherMRSociety").Value, cwrpublisher.MRSocietyId);
                if (string.IsNullOrEmpty(spu.Attribute("PublisherMRSociety").Value))
                    clonedSpu.MRSocietyId = _cf.formatString("099", cwrpublisher.MRSocietyId);

                clonedSpu.SRSocietyId = _cf.formatString("   ", cwrpublisher.SRSocietyId);

                clonedSpu.PROwnershipShare = _cf.formatPercentage(tps.ToString(CultureInfo.InvariantCulture));

                clonedSpu.MROwnershipShare = _cf.formatPercentage(tms.ToString(CultureInfo.InvariantCulture));
                clonedSpu.SROwnershipShare = _cf.formatPercentage("00000");
                clonedSpu.SocietyAssignedAgreementNumber = _cf.formatString(_agreementNumber, cwrpublisher.SocietyAssignedAgreementNumber);
                clonedSpu.PublisherSequence = _cf.formatNumeric(_pubsequence.ToString(), cwrpublisher.PublisherSequence);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return clonedSpu;
        }

        private cwrpublisher GetSubPublisher(cwrpublisher subpublisher)
        {

            var rec = dbEntity.cwrpublisher.FirstOrDefault(aa => aa.Id == subpublisher.Id);
            var clonedSpu = rec;
            try
            {

                clonedSpu.TransactionSequence = _cf.formatNumeric(_transactions.ToString(), subpublisher.TransactionSequence);
                clonedSpu.RecordSequence = _cf.formatNumeric(_records.ToString(), subpublisher.RecordSequence);
                clonedSpu.PRSocietyId = _cf.formatString(_societyPerformingCode, subpublisher.PRSocietyId);
                clonedSpu.MRSocietyId = _cf.formatString(_societyPerformingCode, subpublisher.MRSocietyId);
                clonedSpu.SRSocietyId = _cf.formatString("    ", subpublisher.SRSocietyId);
                clonedSpu.SocietyAssignedAgreementNumber = _cf.formatString(_agreementNumber, subpublisher.SocietyAssignedAgreementNumber);
                clonedSpu.PublisherSequence = _cf.formatNumeric(_pubsequence.ToString(), subpublisher.PublisherSequence);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return clonedSpu;
        }
        #endregion

        #region CoreServicePublishers


        private cwrterritory GetSpt(cwrterritory societyTerritory, XElement spu, XElement spt)
        {

            var rec = dbEntity.cwrterritory.FirstOrDefault(aa => aa.Id == societyTerritory.Id);
            var clonedSpt = rec;
            try
            {

                //Set values to 0 if not found in table.
                var tps = _totalPrShare.ContainsKey(spu.Attribute("PublisherId").Value) ? _totalPrShare[spu.Attribute("PublisherId").Value] : 0.00;
                var tms = _totalMrShare.ContainsKey(spu.Attribute("PublisherId").Value) ? _totalMrShare[spu.Attribute("PublisherId").Value] : 0.00;
                clonedSpt.TransactionSequence = _cf.formatNumeric(_transactions.ToString(), societyTerritory.TransactionSequence);
                clonedSpt.RecordSequence = _cf.formatNumeric(_records.ToString(), societyTerritory.RecordSequence);

                clonedSpt.PRCollectionShare = _cf.formatPercentage(tps.ToString(CultureInfo.InvariantCulture));

                clonedSpt.MRCollectionShare = _cf.formatPercentage(tms.ToString(CultureInfo.InvariantCulture));
                clonedSpt.SRCollectionShare = _cf.formatPercentage("00000");
                clonedSpt.InterestedParty = _cf.formatNumeric(_territoryIp, societyTerritory.InterestedParty);
                clonedSpt.TerritoryCode = _cf.formatNumeric(spt.Attribute("TerritoryCode").Value, societyTerritory.TerritoryCode);
                clonedSpt.Sequence = _cf.formatNumeric(_sequence.ToString(), societyTerritory.Sequence);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return clonedSpt;
        }

        private cwrwriter GetSwr(cwrwriter cwrwriters, XElement ctrlcomposer)
        {

            var rec = dbEntity.cwrwriter.FirstOrDefault(aa => aa.Id == cwrwriters.Id);
            var clonedWriter = rec;

            clonedWriter.TransactionSequence = _cf.formatNumeric(_transactions.ToString(), cwrwriters.TransactionSequence);
            clonedWriter.RecordSequence = _cf.formatNumeric(_records.ToString(), cwrwriters.RecordSequence);
            clonedWriter.InterestedParty = _cf.formatNumeric(ctrlcomposer.Attribute("ComposerId").Value, cwrwriters.InterestedParty);
            clonedWriter.WriterFirstName = _cf.formatString(ctrlcomposer.Attribute("ComposerFirstName").Value + " " + ctrlcomposer.Attribute("ComposerMiddleName").Value, cwrwriters.WriterFirstName);
            clonedWriter.WriterLastName = _cf.formatString(ctrlcomposer.Attribute("ComposerSurname").Value, cwrwriters.WriterLastName);
            clonedWriter.PRShare = _cf.formatPercentage(ctrlcomposer.Attribute("ComposerPOShare").Value);
            clonedWriter.MRShare = _cf.formatPercentage(ctrlcomposer.Attribute("ComposerMOShare").Value);
            clonedWriter.SRShare = _cf.formatPercentage("00000");

            clonedWriter.WriterCAE = _cf.formatString("           ", cwrwriters.WriterCAE);

            if (!string.IsNullOrEmpty(ctrlcomposer.Attribute("ComposerCAE").Value)
                     && ctrlcomposer.Attribute("ComposerCAE").Value.ToLower() != "n/a"
                     && ctrlcomposer.Attribute("ComposerCAE").Value.ToLower() != "tbc")
            {
                clonedWriter.WriterCAE = ctrlcomposer.Attribute("ComposerCAE").Value.PadLeft(11, '0');
            }



            clonedWriter.PRSociety = _cf.formatString(ctrlcomposer.Attribute("ComposerPRSociety").Value, cwrwriters.PRSociety);
            if (string.IsNullOrEmpty(ctrlcomposer.Attribute("ComposerPRSociety").Value))
                clonedWriter.PRSociety = _cf.formatString("099", cwrwriters.PRSociety);

            clonedWriter.MRSociety = _cf.formatString(ctrlcomposer.Attribute("ComposerMRSociety").Value, cwrwriters.MRSociety);
            if (string.IsNullOrEmpty(ctrlcomposer.Attribute("ComposerMRSociety").Value))
                clonedWriter.MRSociety = _cf.formatString("099", cwrwriters.MRSociety);

            clonedWriter.SRSociety = _cf.formatString("   ", cwrwriters.SRSociety);
            clonedWriter.RecordPrefix = "SWR";
            clonedWriter.WriterUnknown = " ";

            return clonedWriter;
        }

        private cwrwriterterritory GetSwt(cwrwriterterritory cwrwriterterritory, XElement ctrlcomposer, XElement swt)
        {

            var rec = dbEntity.cwrwriterterritory.FirstOrDefault(aa => aa.Id == cwrwriterterritory.Id);
            var clonedSwt = rec;

            clonedSwt.TransactionSequence = _cf.formatNumeric(_transactions.ToString(), cwrwriterterritory.TransactionSequence);
            clonedSwt.RecordSequence = _cf.formatNumeric(_records.ToString(), cwrwriterterritory.RecordSequence);
            clonedSwt.PRCollectionShare = _cf.formatPercentage(ctrlcomposer.Attribute("ComposerPOShare").Value);
            clonedSwt.MRCollectionShare = _cf.formatPercentage(ctrlcomposer.Attribute("ComposerMOShare").Value);
            clonedSwt.InterestedParty = _cf.formatNumeric(ctrlcomposer.Attribute("ComposerId").Value, cwrwriterterritory.InterestedParty);
            clonedSwt.TerritoryCode = _cf.formatNumeric(swt.Attribute("TerritoryCode").Value, cwrwriterterritory.TerritoryCode);
            clonedSwt.Sequence = _cf.formatNumeric(_sequence.ToString(), cwrwriterterritory.Sequence);
            return clonedSwt;
        }

        private cwrwriterpublisher GetPwr(cwrwriterpublisher cwrwriterpublisher, XElement ctrlcomposer, XElement originalpub)
        {

            var rec = dbEntity.cwrwriterpublisher.FirstOrDefault(aa => aa.Id == cwrwriterpublisher.Id);
            var clonedWriterPub = rec;

            var pubName = originalpub.Attribute("PublisherName").Value;
            var pubCae = originalpub.Attribute("PublisherCAE").Value;

            clonedWriterPub.TransactionSequence = _cf.formatNumeric(_transactions.ToString(), cwrwriterpublisher.TransactionSequence);
            clonedWriterPub.RecordSequence = _cf.formatNumeric(_records.ToString(), cwrwriterpublisher.RecordSequence);
            clonedWriterPub.WriterIP = _cf.formatNumeric(ctrlcomposer.Attribute("ComposerId").Value, cwrwriterpublisher.WriterIP);
            clonedWriterPub.PublisherIP = _cf.formatNumeric(pubCae, cwrwriterpublisher.PublisherIP);
            clonedWriterPub.PublisherName = _cf.formatString(pubName, cwrwriterpublisher.PublisherName);

            return clonedWriterPub;
        }

        private cwrwriter GetOwr(cwrwriter cwrwriter, XElement nonctrlcomposer)
        {
            var rec = dbEntity.cwrwriter.FirstOrDefault(aa => aa.Id == cwrwriter.Id);
            var clonedWriter = rec;

            clonedWriter.TransactionSequence = _cf.formatNumeric(_transactions.ToString(), cwrwriter.TransactionSequence);
            clonedWriter.InterestedParty = _cf.formatNumeric(nonctrlcomposer.Attribute("ComposerId").Value, cwrwriter.InterestedParty);
            clonedWriter.WriterFirstName = _cf.formatString(nonctrlcomposer.Attribute("ComposerFirstName").Value + " " + nonctrlcomposer.Attribute("ComposerMiddleName").Value, cwrwriter.WriterFirstName);
            clonedWriter.WriterLastName = _cf.formatString(nonctrlcomposer.Attribute("ComposerSurname").Value, cwrwriter.WriterLastName);

            _comPoShare = FormatShare(nonctrlcomposer.Attribute("ComposerPOShare").Value);
            _comMoShare = FormatShare(nonctrlcomposer.Attribute("ComposerMOShare").Value);

            clonedWriter.PRShare = _cf.formatPercentage(_comPoShare.ToString(CultureInfo.InvariantCulture));

            clonedWriter.MRShare = _cf.formatPercentage(_comMoShare.ToString(CultureInfo.InvariantCulture));

            clonedWriter.WriterCAE = _cf.formatNumeric(nonctrlcomposer.Attribute("ComposerCAE").Value, cwrwriter.WriterCAE);

            if (!string.IsNullOrEmpty(nonctrlcomposer.Attribute("ComposerCAE").Value)
                  && nonctrlcomposer.Attribute("ComposerCAE").Value.ToLower() != "n/a"
                  && nonctrlcomposer.Attribute("ComposerCAE").Value.ToLower() != "tbc")
            {
                clonedWriter.WriterCAE = nonctrlcomposer.Attribute("ComposerCAE").Value.PadLeft(11, '0');
            }

            clonedWriter.PRSociety = _cf.formatString(nonctrlcomposer.Attribute("ComposerPRSociety").Value, cwrwriter.PRSociety);
            if (string.IsNullOrEmpty(nonctrlcomposer.Attribute("ComposerPRSociety").Value))
                clonedWriter.PRSociety = _cf.formatString("099", cwrwriter.PRSociety);

            clonedWriter.MRSociety = _cf.formatString(nonctrlcomposer.Attribute("ComposerMRSociety").Value, cwrwriter.MRSociety);
            if (string.IsNullOrEmpty(nonctrlcomposer.Attribute("ComposerMRSociety").Value))
                clonedWriter.MRSociety = _cf.formatString("099", cwrwriter.MRSociety);


            clonedWriter.SRSociety = _cf.formatString("   ", cwrwriter.SRSociety);
            clonedWriter.RecordSequence = _cf.formatNumeric(_records.ToString(), cwrwriter.RecordSequence);
            clonedWriter.RecordPrefix = "OWR";
            clonedWriter.WriterUnknown = " ";

            return clonedWriter;
        }

        private cwrrecordformat GetRev(cwrrecordformat cwrrev, XElement work)
        {

            var rec = Getcwrrecordformat(2);//by default Rev line format is set to 2 
            var clonedRev = rec;

            clonedRev.TransactionSequence = _cf.formatNumeric(_transactions.ToString(), cwrrev.TransactionSequence);
            clonedRev.WorkId = _cf.formatNumeric(work.Attribute("Id").Value, cwrrev.WorkId);
            clonedRev.WorkTitle = _cf.formatString(work.Attribute("Title").Value, cwrrev.WorkTitle);
            clonedRev.ISWC = _cf.formatNumeric(work.Attribute("ISWC").Value, cwrrev.ISWC);
            clonedRev.Duration = _cf.formatString(work.Attribute("DurationHH").Value + work.Attribute("DurationMM").Value + work.Attribute("DurationSS").Value, cwrrev.Duration);
            clonedRev.CatalogueNumber = _cf.formatString(work.Attribute("CatalogNumber").Value, cwrrev.CatalogueNumber);

            return clonedRev;
        }
        #endregion
        private double FormatShare(string share)
        {
            double percentage = 0;

            if (!string.IsNullOrEmpty(share))
                percentage = double.Parse(share);

            return percentage;
        }

        #region DBCALLS
        private cwrtemplate GetCwrTemplate(int templateId)
        {
            var rec = dbEntity.cwrtemplate.FirstOrDefault(aa => aa.Id == templateId);
            return rec;
        }
        private cwrrecordformat Getcwrrecordformat(int id)
        {
            var rec = dbEntity.cwrrecordformat.FirstOrDefault(aa => aa.Id == id);
            return rec;
        }

        private cwrpublisher Getcwrpublisher(int id)
        {
            var rec = dbEntity.cwrpublisher.FirstOrDefault(aa => aa.Id == id);
            return rec;
        }

        private cwrterritory Getcwrterritory(int id)
        {

            var rec = dbEntity.cwrterritory.FirstOrDefault(aa => aa.Id == id);
            return rec;
        }

        private cwrwriter Getcwrwriter(int id)
        {

            var rec = dbEntity.cwrwriter.FirstOrDefault(aa => aa.Id == id);
            return rec;
        }

        private cwrwriterpublisher Getcwrwriterpublisher(int id)
        {

            var rec = dbEntity.cwrwriterpublisher.FirstOrDefault(aa => aa.Id == id);
            return rec;
        }

        private cwrwriterterritory Getcwrwriterterritory(int id)
        {

            var rec = dbEntity.cwrwriterterritory.FirstOrDefault(aa => aa.Id == id);
            return rec;
        }



        #endregion
        #endregion

    }
}
